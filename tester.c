#include "Game.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

void Resources(Game g, int playerID){
	int i = 0;
	while(i<6){
		printf("%d : %d\n",i,getStudents(g,playerID,i));
		i++;
	}
}

int main(int argc, char const *argv[])
{
action a;
int disciplines[] = {2,5,3,5,3,1,4,4,1,4,2,3,2,0,3,5,4,2,1};
int dice[] = {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5};
Game g = newGame(disciplines, dice);
throwDice(g, 6);
a.actionCode = OBTAIN_ARC;
strncpy(a.destination, "LRR", PATH_LIMIT - 1);
a.destination[PATH_LIMIT - 1] = 0;
a.disciplineFrom = -1, a.disciplineTo = -1;
makeAction(g, a);
a.actionCode = OBTAIN_ARC;
strncpy(a.destination, "LRR", PATH_LIMIT - 1);
a.destination[PATH_LIMIT - 1] = 0;
a.disciplineFrom = -1, a.disciplineTo = -1;
isLegalAction(g, a);
return 0;
}

