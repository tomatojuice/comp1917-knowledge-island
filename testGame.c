#include "Game.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#define MARK printf("MARKED \n");
#define BOARD_REGIONS 19

#define DEFAULT_DISCIPLINES { \
				STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, \
				STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, \
				STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, STUDENT_MJ, \
				STUDENT_BQN, STUDENT_THD, STUDENT_MJ, STUDENT_MMONEY,\
				STUDENT_MTV, STUDENT_BQN, STUDENT_BPS}
#define DEFAULT_DICE {\
				9,10,8,\
				12,6,5,3,\
				11,3,11,4,6,\
				4,7,9,2,\
				8,10,5}

static void testGame();

int main(int argc, char *argv[]) {
	//printf("Main has begun!\n");
	testGame();
	return EXIT_SUCCESS;
}

static void testGame() {
  //printf("Tests have begun!\n");
  int i,j,k;
  action a;
  //intiialize and check newGame
  int disciplines[] = DEFAULT_DISCIPLINES;
  int dice[] = DEFAULT_DICE;
	Game g = newGame(disciplines,dice);
	assert(g!=NULL);
	//printf("Game is created\n");
  

	//############################
	//empty game or Terra Nullis
	//############################
		//getDiscipline get getDiceValue
		i = 0;
		while(i<NUM_REGIONS){
			assert(getDiscipline(g,i) == disciplines[i]);
			assert(getDiceValue(g,i) == dice[i]);
			i++;
		}
	//	printf("First while loop escaped\n");

		assert(getMostARCs(g) == NO_ONE);
		assert(getMostPublications(g) == NO_ONE);
		assert(getTurnNumber(g) == -1);
		assert(getWhoseTurn(g) == NO_ONE);
		
		//player functions	
		i = UNI_A;
		while(i<=UNI_C){
			assert(getKPIpoints(g,i)==20);
			assert(getARCs(g,i)==0);
			assert(getGO8s(g,i)==0);
			assert(getCampuses(g,i)==2);
			assert(getIPs(g,i)==0);
			assert(getPublications(g,i)==0);

			assert(getStudents(g,i,STUDENT_THD)==0);
			assert(getStudents(g,i,STUDENT_BPS)==3);
			assert(getStudents(g,i,STUDENT_BQN)==3);
			assert(getStudents(g,i,STUDENT_MJ)==1);
			assert(getStudents(g,i,STUDENT_MTV)==1);
			assert(getStudents(g,i,STUDENT_MMONEY)==1);
			
			j = 1;
			while(j<6){
				k=0;
				while(k<6){
					if(k!=j){
						assert(getExchangeRate(g,i,j,k)==3);
					}
					k++;
				}
				j++;
			}
			i++;
		}

	//printf("Second while in while loop passed \n");
	//terra nullis over

	throwDice(g,6);
	a.actionCode = PASS;
		strcpy(a.destination,"");
		a.disciplineFrom = STUDENT_THD;
		a.disciplineTo = STUDENT_THD;
		assert(isLegalAction(g,a)==TRUE);

	action a1;
	a1.actionCode=OBTAIN_ARC;
	a1.destination[0]='L';
	a1.destination[1]='\0';
	assert(isLegalAction(g,a1)==TRUE);
	makeAction(g,a1);
	a1.actionCode = BUILD_GO8;
	strcpy(a1.destination,"L");
	a1.disciplineTo = 0;
	a1.disciplineTo = 0;
	assert(isLegalAction(g,a1)==FALSE);
	
	//check isLegal(not finished)
	
	assert(getMostARCs(g) == UNI_A);
	assert(getMostPublications(g) == NO_ONE);
	assert(getTurnNumber(g) == 0);
	assert(getWhoseTurn(g) == UNI_A);
	
	//printf("%d\n", getKPIpoints(g,UNI_A));

	assert(getKPIpoints(g,UNI_A)==32); //Having the most ARC grants so get 10 KPIs
	assert(getARCs(g,UNI_A)==1);
	assert(getGO8s(g,UNI_A)==0);
	assert(getCampuses(g,UNI_A)==2);
	assert(getIPs(g,UNI_A)==0);
	assert(getPublications(g,UNI_A)==0);
	
	assert(getStudents(g,UNI_A,STUDENT_THD)==0);
	assert(getStudents(g,UNI_A,STUDENT_BPS)==2);
	assert(getStudents(g,UNI_A,STUDENT_BQN)==2);
	assert(getStudents(g,UNI_A,STUDENT_MJ)==2);
	assert(getStudents(g,UNI_A,STUDENT_MTV)==1);
	assert(getStudents(g,UNI_A,STUDENT_MMONEY)==1);
	
	i=UNI_B;
	while(i<=UNI_C){
		assert(getKPIpoints(g,i)==20);
		assert(getARCs(g,i)==0);
		assert(getGO8s(g,i)==0);
		assert(getCampuses(g,i)==2);
		assert(getIPs(g,i)==0);
		assert(getPublications(g,i)==0);
		
		assert(getStudents(g,i,STUDENT_THD)==0);
		assert(getStudents(g,i,STUDENT_BPS)==3);
		assert(getStudents(g,i,STUDENT_BQN)==3);
		assert(getStudents(g,i,STUDENT_MJ)==1);
		assert(getStudents(g,i,STUDENT_MTV)==1);
		assert(getStudents(g,i,STUDENT_MMONEY)==1);
		
		j = 1;
		while(j<6){
			k=0;
			while(k<6){
				if(k!=j){
					assert(getExchangeRate(g,i,j,k)==3);
				}
				k++;
			}
			j++;
		}
		i++;
	}
	disposeGame(g);

	printf("Jabez's tests have passed!\n");


	//########################################
	// Paulus' test functions
	//########################################

	/*
	 * test for:
	 * void disposeGame (Game g);
	 */

	// N/A
	
	/*
	 * test for:
	 * void makeAction (Game g, action a);
	 */

	// N/A

	/*
	 * test for:
	 * void throwDice (Game g, int diceScore);
	 */

	// N/A
	
	/*
	 * test for:
	 *int getDiscipline (Game g, int regionID);
	 */

	Game gameForGetDisciplineTest;
	//disciplines = DEFAULT_DISCIPLINES;
	// All THD disciplines
	int disciplinesTHD[] = {
		STUDENT_THD, STUDENT_THD, STUDENT_THD, STUDENT_THD,
		STUDENT_THD, STUDENT_THD, STUDENT_THD, STUDENT_THD,
		STUDENT_THD, STUDENT_THD, STUDENT_THD, STUDENT_THD,
		STUDENT_THD, STUDENT_THD, STUDENT_THD, STUDENT_THD,
		STUDENT_THD, STUDENT_THD, STUDENT_THD
	};

	// All BPS disciplines
	int disciplinesBPS[] = {
		STUDENT_BPS, STUDENT_BPS, STUDENT_BPS, STUDENT_BPS,
		STUDENT_BPS, STUDENT_BPS, STUDENT_BPS, STUDENT_BPS,
		STUDENT_BPS, STUDENT_BPS, STUDENT_BPS, STUDENT_BPS,
		STUDENT_BPS, STUDENT_BPS, STUDENT_BPS, STUDENT_BPS,
		STUDENT_BPS, STUDENT_BPS, STUDENT_BPS
	};

	// All BQN disciplines
	int disciplinesBQN[] = {
		STUDENT_BQN, STUDENT_BQN, STUDENT_BQN, STUDENT_BQN,
		STUDENT_BQN, STUDENT_BQN, STUDENT_BQN, STUDENT_BQN,
		STUDENT_BQN, STUDENT_BQN, STUDENT_BQN, STUDENT_BQN,
		STUDENT_BQN, STUDENT_BQN, STUDENT_BQN, STUDENT_BQN,
		STUDENT_BQN, STUDENT_BQN, STUDENT_BQN
	};

	// All MJ disciplines
	int disciplinesMJ[] = {
		STUDENT_MJ, STUDENT_MJ, STUDENT_MJ, STUDENT_MJ,
		STUDENT_MJ, STUDENT_MJ, STUDENT_MJ, STUDENT_MJ,
		STUDENT_MJ, STUDENT_MJ, STUDENT_MJ, STUDENT_MJ,
		STUDENT_MJ, STUDENT_MJ, STUDENT_MJ, STUDENT_MJ,
		STUDENT_MJ, STUDENT_MJ, STUDENT_MJ
	};

	// All MMONEY disciplines
	int disciplinesMMONEY[] = {
		STUDENT_MMONEY, STUDENT_MMONEY, STUDENT_MMONEY, STUDENT_MMONEY,
		STUDENT_MMONEY, STUDENT_MMONEY, STUDENT_MMONEY, STUDENT_MMONEY,
		STUDENT_MMONEY, STUDENT_MMONEY, STUDENT_MMONEY, STUDENT_MMONEY,
		STUDENT_MMONEY, STUDENT_MMONEY, STUDENT_MMONEY, STUDENT_MMONEY,
		STUDENT_MMONEY, STUDENT_MMONEY, STUDENT_MMONEY
	};

	// All MTV disciplines
	int disciplinesMTV[] = {
		STUDENT_MTV, STUDENT_MTV, STUDENT_MTV, STUDENT_MTV,
		STUDENT_MTV, STUDENT_MTV, STUDENT_MTV, STUDENT_MTV,
		STUDENT_MTV, STUDENT_MTV, STUDENT_MTV, STUDENT_MTV,
		STUDENT_MTV, STUDENT_MTV, STUDENT_MTV, STUDENT_MTV,
		STUDENT_MTV, STUDENT_MTV, STUDENT_MTV
	};

	// Custom disciplines
	int disciplinesCustom[] = {
		STUDENT_MTV, STUDENT_THD, STUDENT_MJ, STUDENT_MMONEY,
		STUDENT_BPS, STUDENT_BPS, STUDENT_BQN, STUDENT_MJ,
		STUDENT_THD, STUDENT_MTV, STUDENT_THD, STUDENT_MMONEY,
		STUDENT_BQN, STUDENT_BQN, STUDENT_MJ, STUDENT_MTV,
		STUDENT_BPS, STUDENT_THD, STUDENT_THD
	};




	gameForGetDisciplineTest = newGame(disciplines, dice);

	// Check all regions in the board
	int counterForGetDisciplineTest = 0;
	while (counterForGetDisciplineTest < BOARD_REGIONS) {
		assert(getDiscipline(gameForGetDisciplineTest, counterForGetDisciplineTest) ==
			disciplines[counterForGetDisciplineTest]);
		counterForGetDisciplineTest++;
	}

    disposeGame(gameForGetDisciplineTest);
	gameForGetDisciplineTest = newGame(disciplinesTHD, dice);

	// Check all regions in the board
	counterForGetDisciplineTest = 0;
	while (counterForGetDisciplineTest < BOARD_REGIONS) {
		assert(getDiscipline(gameForGetDisciplineTest, counterForGetDisciplineTest) ==
			STUDENT_THD);
		counterForGetDisciplineTest++;
	}
    disposeGame(gameForGetDisciplineTest);
	gameForGetDisciplineTest = newGame(disciplinesBPS, dice);

	// Check all regions in the board
	counterForGetDisciplineTest = 0;
	while (counterForGetDisciplineTest < BOARD_REGIONS) {
		assert(getDiscipline(gameForGetDisciplineTest, counterForGetDisciplineTest) ==
			STUDENT_BPS);
		counterForGetDisciplineTest++;
	}
    disposeGame(gameForGetDisciplineTest);
	gameForGetDisciplineTest = newGame(disciplinesBQN, dice);

	// Check all regions in the board
	counterForGetDisciplineTest = 0;
	while (counterForGetDisciplineTest < BOARD_REGIONS) {
		assert(getDiscipline(gameForGetDisciplineTest, counterForGetDisciplineTest) ==
			STUDENT_BQN);
		counterForGetDisciplineTest++;
	}
    disposeGame(gameForGetDisciplineTest);
	gameForGetDisciplineTest = newGame(disciplinesMJ, dice);

	// Check all regions in the board
	counterForGetDisciplineTest = 0;
	while (counterForGetDisciplineTest < BOARD_REGIONS) {
		assert(getDiscipline(gameForGetDisciplineTest, counterForGetDisciplineTest) ==
			STUDENT_MJ);
		counterForGetDisciplineTest++;
	}
    disposeGame(gameForGetDisciplineTest);
	gameForGetDisciplineTest = newGame(disciplinesMMONEY, dice);

	// Check all regions in the board
	counterForGetDisciplineTest = 0;
	while (counterForGetDisciplineTest < BOARD_REGIONS) {
		assert(getDiscipline(gameForGetDisciplineTest, counterForGetDisciplineTest) ==
			STUDENT_MMONEY);
		counterForGetDisciplineTest++;
	}
    disposeGame(gameForGetDisciplineTest);
	gameForGetDisciplineTest = newGame(disciplinesMTV, dice);

	// Check all regions in the board
	counterForGetDisciplineTest = 0;
	while (counterForGetDisciplineTest < BOARD_REGIONS) {
		assert(getDiscipline(gameForGetDisciplineTest, counterForGetDisciplineTest) ==
			STUDENT_MTV);
		counterForGetDisciplineTest++;
	}
    disposeGame(gameForGetDisciplineTest);
	gameForGetDisciplineTest = newGame(disciplinesCustom, dice);

	// Check all regions in the board
	counterForGetDisciplineTest = 0;
	while (counterForGetDisciplineTest < BOARD_REGIONS) {
	assert(getDiscipline(gameForGetDisciplineTest, counterForGetDisciplineTest) ==
			disciplinesCustom[counterForGetDisciplineTest]);
		counterForGetDisciplineTest++;
	}
	
	/*
	 * test for:
	 * int getDiceValue (Game g, int regionID);
	 */
    disposeGame(gameForGetDisciplineTest);
	Game gameForGetDiceValue;

	int diceCustom1[] = {
		3, 1, 4, 1,
		5, 9, 2, 6,
		5, 3, 5, 8,
		9, 7, 9, 3,
		2, 3, 8
	};

	int diceCustom2[] = {
		6, 2, 6, 4,
		3, 3, 8, 3,
		2, 7, 9, 5,
		0, 2, 8, 8,
		4, 1, 9
	};
	int diceCustom3[] = {
		10, 11, 12, 0,
		10, 11, 12, 0,
		10, 11, 12, 0,
		10, 11, 12, 0,
		11, 12, 0
	};

	gameForGetDiceValue = newGame(disciplines, dice);

	// Check all regions in the board
	int counterForGetDiceValue = 0;
	while (counterForGetDiceValue < BOARD_REGIONS) {
		assert(getDiceValue(gameForGetDiceValue,
			counterForGetDiceValue) ==
			dice[counterForGetDiceValue]);
		counterForGetDiceValue++;
	}
    disposeGame(gameForGetDiceValue);
	gameForGetDiceValue = newGame(disciplines, diceCustom1);

	// Check all regions in the board
	counterForGetDiceValue = 0;
	while (counterForGetDiceValue < BOARD_REGIONS) {
		assert(getDiceValue(gameForGetDiceValue,
			counterForGetDiceValue) ==
			diceCustom1[counterForGetDiceValue]);
		counterForGetDiceValue++;
	}
    disposeGame(gameForGetDiceValue);
	gameForGetDiceValue = newGame(disciplines, diceCustom2);

	// Check all regions in the board
	counterForGetDiceValue = 0;
	while (counterForGetDiceValue < BOARD_REGIONS) {
		assert(getDiceValue(gameForGetDiceValue,
			counterForGetDiceValue) ==
			diceCustom2[counterForGetDiceValue]);
		counterForGetDiceValue++;
	}
    disposeGame(gameForGetDiceValue);
	gameForGetDiceValue = newGame(disciplines, diceCustom3);

	// Check all regions in the board
	counterForGetDiceValue = 0;
	while (counterForGetDiceValue < BOARD_REGIONS) {
		assert(getDiceValue(gameForGetDiceValue,
			counterForGetDiceValue) ==
			diceCustom3[counterForGetDiceValue]);
		counterForGetDiceValue++;
	}
	
	/*
	 * test for:
	 * int getMostARCs (Game g);
	 */


	// This needs more work
    disposeGame(gameForGetDiceValue);
	// Simulates an 11 roll game, dice rolls: 2...11
	Game gameForGetMostARCs = newGame(disciplines, dice);
	assert(gameForGetDiceValue != NULL);
	int diceScore = 2;
	int turnNumber = 0;
	int whoseTurn = UNI_A;
	i = UNI_A;

	assert(getMostARCs(gameForGetMostARCs) == NO_ONE);
    


	// while (diceScore <= 12) {
	// 	throwDice (g, diceScore);

	// 	//cycles through the players each time turnNumber is a multiple of 3
	// 	whoseTurn = (turnNumber%NUM_UNIS)+1;
	// 	//test the easy getter functions
	// 	assert(getWhoseTurn(g) == whoseTurn);
	// 	assert(getTurnNumber(g) == turnNumber);
	// 	//nobody has made any actions
	// 	while(i<=UNI_C){
	// 		assert(getKPIpoints(g,i)==0);
	// 		i++;
	// 	}
	// 	assert(getMostPublications(g) == NO_ONE);



	// 	diceScore++;
	// 	turnNumber++;
	// }

    disposeGame(gameForGetMostARCs);
	//########################################
	// End Paulus' test functions
	//########################################


	//####################
	//Matt's test functions
	//####################
   
	Game MattGame = newGame(disciplines,dice);
	assert(g!=NULL);
	//Changes the game state 11 times
	//DiceScore thrown 2,3,4...,12
	diceScore = 2;
	turnNumber = 0;
	whoseTurn = UNI_A;
	i = UNI_A;
		
	
	while (diceScore <= 11) {
		throwDice (MattGame, diceScore);

		//cycles through the players each time turnNumber is a multiple of 3
		whoseTurn = (turnNumber%NUM_UNIS)+1;
		//test the easy getter functions
		assert(getWhoseTurn(MattGame) == whoseTurn);
		assert(getTurnNumber(MattGame) == turnNumber);
		//nobody has made any actions
		i=UNI_A;
		while(i<=UNI_C){
			assert(getKPIpoints(MattGame,i)==20);
			i++;
		}
		assert(getMostPublications(MattGame) == NO_ONE);



		diceScore++;
		turnNumber++;
	}

	printf("Matt's getter functions passed after game state change!\n");

	//Test the getCampus and getARC

	path pathToVertex;
	strcpy(pathToVertex, "");

	assert(getCampus(MattGame, pathToVertex) == CAMPUS_A);

	//Path to the vertex is an array with L for Left, R for Right and B for back.
	//How are we going to represent that? 
	//Not sure whether the 0 at the end is meant to be a 0 or a \0 
	strcpy(pathToVertex,"R");
	//pathToVertex[1] = '\0';
	assert(getCampus(MattGame, pathToVertex) == VACANT_VERTEX);
	assert(getARC(MattGame,pathToVertex) == VACANT_ARC);

	strcpy(pathToVertex, "RRLRL");
	//pathToVertex[0] = 'R';
	//pathToVertex[1] = 'R';
	//pathToVertex[2] = 'L';
	//pathToVertex[3] = 'R';
	//pathToVertex[4] = 'L';
	//pathToVertex[5] = '\0';

	assert(getCampus(MattGame, pathToVertex) == CAMPUS_B);
	assert(getARC(MattGame, pathToVertex) == VACANT_ARC);

	strcpy(pathToVertex, "RB");
	//pathToVertex[0] = 'R';
	//pathToVertex[1] ='B'; 
	//pathToVertex[2] = '\0';
	assert(getCampus(MattGame, pathToVertex) == CAMPUS_A);
	assert(getARC(MattGame, pathToVertex) == VACANT_ARC);


	printf("Matt's getCampus and getArc functions passed!\n");

	disposeGame(MattGame);


	MattGame = newGame(disciplines,dice);
	assert(MattGame != NULL);


	action exampleActionMatt;
	exampleActionMatt.actionCode = OBTAIN_ARC; 
	strcpy(exampleActionMatt.destination, "R");
	//exampleActionMatt.destination[0] = 'R';
	//exampleActionMatt.destination[1] = '\0';
	exampleActionMatt.disciplineFrom = 6;
	exampleActionMatt.disciplineTo = 6;
	//printf("Right arc is owned by : %d\n", getARC(MattGame, exampleActionMatt.destination));
	
	diceScore = 2;
	int count = 0;

	while (count < 3){

		
		if(getWhoseTurn(MattGame)==UNI_A){
			
			//assert (getCampus(MattGame, exampleActionMatt.destination) == CAMPUS_A);
	//		printf("Right arc is owned by : %d\n", getARC(MattGame, exampleActionMatt.destination));
			assert (getARC(MattGame, exampleActionMatt.destination) == VACANT_ARC);

			assert(isLegalAction(MattGame,exampleActionMatt) == TRUE);
			makeAction(MattGame,exampleActionMatt);

			assert(getKPIpoints(MattGame, UNI_A) == 32);
			assert(getKPIpoints(MattGame, UNI_B) == 20);
			assert(getKPIpoints(MattGame, UNI_C) == 20);

			assert(getStudents(MattGame, UNI_A, STUDENT_BPS) == 2);
			assert(getStudents(MattGame, UNI_A, STUDENT_BQN) == 2);

			assert (getARC(MattGame, exampleActionMatt.destination) == ARC_A);
			count++;
	
		}else if (getWhoseTurn(MattGame) == UNI_B){
			strcpy(exampleActionMatt.destination, "RRLRL");
			//exampleActionMatt.destination[0] = 'R';
			//exampleActionMatt.destination[1] = 'R';
			//exampleActionMatt.destination[2] = 'L';
			//exampleActionMatt.destination[3] = 'R';
			//exampleActionMatt.destination[4] = 'L';
			//exampleActionMatt.destination[5] = '\0';
			
			assert (getCampus(MattGame, exampleActionMatt.destination) == CAMPUS_B);
			assert (getARC(MattGame, exampleActionMatt.destination) == VACANT_ARC);

			assert(isLegalAction(MattGame,exampleActionMatt) == TRUE);
			
	//		printf("Before action, NUM BQN = %d\n", getStudents(MattGame, UNI_B, STUDENT_BQN));
			makeAction(MattGame,exampleActionMatt);


			assert(getKPIpoints(MattGame, UNI_A) == 32);
			assert(getKPIpoints(MattGame, UNI_B) == 22);
			assert(getKPIpoints(MattGame, UNI_C) == 20);

	//		printf("Num BQN = %d\n", getStudents(MattGame, UNI_B, STUDENT_BQN));
			assert(getStudents(MattGame, UNI_B, STUDENT_BPS) == 2);
			assert(getStudents(MattGame, UNI_B, STUDENT_BQN) == 2);

			assert (getARC(MattGame, exampleActionMatt.destination) == ARC_B);

			count++;

		}else if (getWhoseTurn(MattGame)==UNI_C){
			strcpy(exampleActionMatt.destination, "LRLRL");
			//exampleActionMatt.destination[0] = 'L'; 
			//exampleActionMatt.destination[1] = 'R';
			//exampleActionMatt.destination[2] = 'L';
			//exampleActionMatt.destination[3] = 'R';
			//exampleActionMatt.destination[4] = 'L';
			//exampleActionMatt.destination[5] = '\0';
			
			assert (getCampus(MattGame, exampleActionMatt.destination) == CAMPUS_C);
			assert (getARC(MattGame, exampleActionMatt.destination) == VACANT_ARC);
			
			assert(isLegalAction(MattGame,exampleActionMatt) == TRUE);
			makeAction(MattGame,exampleActionMatt);

	//		printf("UNI_A KPI = %d\n", getKPIpoints(MattGame, UNI_A) );
	//		printf("UNI_C numArcs = %d\n", getARCs(MattGame,UNI_C) );
			assert(getKPIpoints(MattGame, UNI_A) == 32);
			assert(getKPIpoints(MattGame, UNI_B) == 22);
			assert(getKPIpoints(MattGame, UNI_C) == 22);
			assert(getStudents(MattGame, UNI_C, STUDENT_BPS) == 2);
			assert(getStudents(MattGame, UNI_C, STUDENT_BQN) == 2);

			assert (getARC(MattGame, exampleActionMatt.destination) == ARC_C);

			count++;

		}
		 

		assert(getMostPublications(MattGame) == NO_ONE);

		throwDice(MattGame, diceScore);
	}


	printf("The getKPI and getMostPublications test passed!\n");


	//Test isLegalAction false & the PASS action

	strcpy(exampleActionMatt.destination, "B");
	//exampleActionMatt.destination[1] = '\0';
	assert(isLegalAction(MattGame,exampleActionMatt) == FALSE);
	strcpy(exampleActionMatt.destination, "LRL");
	//exampleActionMatt.destination[0] = 'L';
	//exampleActionMatt.destination[1] = 'R';
	//exampleActionMatt.destination[2] = 'L';
	//exampleActionMatt.destination[3] = '\0';
	assert(isLegalAction(MattGame,exampleActionMatt) == FALSE);
	exampleActionMatt.actionCode = BUILD_GO8;
	assert(isLegalAction(MattGame,exampleActionMatt) == FALSE);
	exampleActionMatt.actionCode = START_SPINOFF;
	assert(isLegalAction(MattGame,exampleActionMatt) == TRUE);
	exampleActionMatt.actionCode = RETRAIN_STUDENTS;
	assert(isLegalAction(MattGame,exampleActionMatt) == FALSE);
	exampleActionMatt.actionCode = PASS;
	assert(isLegalAction(MattGame,exampleActionMatt) == TRUE);

	printf("isLegalAction tests passed!\n");

	disposeGame(MattGame);



    
    g = newGame(disciplines,dice);
	assert(g!=NULL);

	throwDice(g,2);
	//printf ("number of BQN after first throwDice action = %d\n",getStudents(g,UNI_B,STUDENT_BQN));
	action a2;
	a2.actionCode=OBTAIN_ARC;
	a2.destination[0]= 'L';
	a2.destination[1]= '\0';
	//a2.destination[2]= '\0';
	makeAction(g,a2);
	
	//check isLegal(not finished)
	//printf("getMostArcs = %d\n", getMostARCs(g));	
	assert(getMostARCs(g) == UNI_A);
	assert(getMostPublications(g) == NO_ONE);
	assert(getTurnNumber(g) == 0);
	assert(getWhoseTurn(g) == UNI_A);
	
	assert(getKPIpoints(g,UNI_A)==32);
	assert(getARCs(g,UNI_A)==1);
	assert(getGO8s(g,UNI_A)==0);
	assert(getCampuses(g,UNI_A)==2);
	assert(getIPs(g,UNI_A)==0);
	assert(getPublications(g,UNI_A)==0);
	
	
	assert(getStudents(g,UNI_A,STUDENT_THD)==0);
	assert(getStudents(g,UNI_A,STUDENT_BPS)==2);
	assert(getStudents(g,UNI_A,STUDENT_BQN)==2);
	assert(getStudents(g,UNI_A,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_A,STUDENT_MTV)==1);
	assert(getStudents(g,UNI_A,STUDENT_MMONEY)==1);
	
	i=UNI_B;
	while(i<=UNI_C){
		assert(getKPIpoints(g,i)==20);
		assert(getARCs(g,i)==0);
		assert(getGO8s(g,i)==0);
		assert(getCampuses(g,i)==2);
		assert(getIPs(g,i)==0);
		assert(getPublications(g,i)==0);
		
		assert(getStudents(g,i,STUDENT_THD)==0);
		assert(getStudents(g,i,STUDENT_BPS)==3);
		assert(getStudents(g,i,STUDENT_BQN)==3);
		assert(getStudents(g,i,STUDENT_MJ)==1);
		assert(getStudents(g,i,STUDENT_MTV)==1);
		assert(getStudents(g,i,STUDENT_MMONEY)==1);
		
		j = 1;
		while(j<6){
			k=0;
			while(k<6){
				if(k!=j){
					assert(getExchangeRate(g,i,j,k)==3);
				}
			k++;	
			}
			j++;
		}
		i++;
	}

	a2.destination[0] = 'L';
	a2.destination[1] = 'L';
	a2.destination[2] = '\0';
	assert(isLegalAction(MattGame, a2)==FALSE);


	action a3;
	a3.actionCode=OBTAIN_ARC;
	a3.destination[0]='L';
	a3.destination[1]='R';
	a3.destination[2]='\0';
	makeAction(g,a3);

	//printf("Action a3 made!\n");
	
	//check isLegal(not finished)
	//printf("most Arcs %d \n", getMostARCs(g));
	assert(getMostARCs(g) == UNI_A);
	assert(getMostPublications(g) == NO_ONE);
	assert(getTurnNumber(g) == 0);
	assert(getWhoseTurn(g) == UNI_A);
	
	assert(getKPIpoints(g,UNI_A)==34);
	assert(getARCs(g,UNI_A)==2);
	assert(getGO8s(g,UNI_A)==0);
	assert(getCampuses(g,UNI_A)==2);
	assert(getIPs(g,UNI_A)==0);
	assert(getPublications(g,UNI_A)==0);
	
	
	assert(getStudents(g,UNI_A,STUDENT_THD)==0);
	assert(getStudents(g,UNI_A,STUDENT_BPS)==1);
	assert(getStudents(g,UNI_A,STUDENT_BQN)==1);
	assert(getStudents(g,UNI_A,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_A,STUDENT_MTV)==1);
	assert(getStudents(g,UNI_A,STUDENT_MMONEY)==1);
	
	i=UNI_B;
	while(i<=UNI_C){
		assert(getKPIpoints(g,i)==20);
		assert(getARCs(g,i)==0);
		assert(getGO8s(g,i)==0);
		assert(getCampuses(g,i)==2);
		assert(getIPs(g,i)==0);
		assert(getPublications(g,i)==0);
		
		assert(getStudents(g,i,STUDENT_THD)==0);
		assert(getStudents(g,i,STUDENT_BPS)==3);
		assert(getStudents(g,i,STUDENT_BQN)==3);
		assert(getStudents(g,i,STUDENT_MJ)==1);
		assert(getStudents(g,i,STUDENT_MTV)==1);
		assert(getStudents(g,i,STUDENT_MMONEY)==1);
		
		j = 1;
		while(j<6){
			k=0;
			while(k<6){
				if(k!=j){
					assert(getExchangeRate(g,i,j,k)==3);
				}
			k++;
			}
			j++;
		}
		i++;
	}

	//printf("Second while passed!\n");


	throwDice(g,2);
	//action a1;
	a1.actionCode=OBTAIN_ARC;
	a1.destination[0]='R';
	a1.destination[1]='R';
	a1.destination[2]='L';
	a1.destination[3]='R';
	a1.destination[4]='L';
	a1.destination[5]='\0';
	
	makeAction(g,a1);
	//printf("Action a2 has been processed!");
	
	assert(getMostARCs(g) == UNI_A);
	assert(getMostPublications(g) == NO_ONE);
	assert(getTurnNumber(g) == 1);
	assert(getWhoseTurn(g) == UNI_B);
	
	assert(getKPIpoints(g,UNI_B)==22);
	assert(getARCs(g,UNI_B)==1);
	assert(getGO8s(g,UNI_B)==0);
	assert(getCampuses(g,UNI_B)==2);
	assert(getIPs(g,UNI_B)==0);
	assert(getPublications(g,UNI_B)==0);
	
	assert(getStudents(g,UNI_B,STUDENT_THD)==0);
	assert(getStudents(g,UNI_B,STUDENT_BPS)==2);
	assert(getStudents(g,UNI_B,STUDENT_BQN)==2);
	assert(getStudents(g,UNI_B,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_B,STUDENT_MTV)==1);
	assert(getStudents(g,UNI_B,STUDENT_MMONEY)==1);
	
	//check UNI_A is not change
	assert(getKPIpoints(g,UNI_A)==34);
	assert(getARCs(g,UNI_A)==2);
	assert(getGO8s(g,UNI_A)==0);
	assert(getCampuses(g,UNI_A)==2);
	assert(getIPs(g,UNI_A)==0);
	assert(getPublications(g,UNI_A)==0);
	
	
	assert(getStudents(g,UNI_A,STUDENT_THD)==0);
	assert(getStudents(g,UNI_A,STUDENT_BPS)==1);
	assert(getStudents(g,UNI_A,STUDENT_BQN)==1);
	assert(getStudents(g,UNI_A,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_A,STUDENT_MTV)==1);
	assert(getStudents(g,UNI_A,STUDENT_MMONEY)==1);
	
	//check UNI_C is not change
	assert(getKPIpoints(g,UNI_C)==20);
	assert(getARCs(g,UNI_C)==0);
	assert(getGO8s(g,UNI_C)==0);
	assert(getCampuses(g,UNI_C)==2);
	assert(getIPs(g,UNI_C)==0);
	assert(getPublications(g,UNI_C)==0);
	
	assert(getStudents(g,UNI_C,STUDENT_THD)==0);
	assert(getStudents(g,UNI_C,STUDENT_BPS)==3);
	assert(getStudents(g,UNI_C,STUDENT_BQN)==3);
	assert(getStudents(g,UNI_C,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_C,STUDENT_MTV)==1);
	assert(getStudents(g,UNI_C,STUDENT_MMONEY)==1);
	
	//action a2;
	//printf("Who owns RRLRL = %d\n", getARC(g,"RRLRL"));
	//printf("Whose turn = %d\n", getWhoseTurn(g));
	a1.destination[0]='R';
	a1.destination[1]='R';
	a1.destination[2]='L';
	a1.destination[3]='R';
	a1.destination[4]='R';
	a1.destination[5]='\0';
	assert(isLegalAction(g,a1)==FALSE);

	a2.actionCode=OBTAIN_ARC;
	a2.destination[0]='R';
	a2.destination[1]='R';
	a2.destination[2]='L';
	a2.destination[3]='R';
	//a2.destination[4]='L';
	//a2.destination[5]='B';
	a2.destination[4]='\0';
	assert(isLegalAction(g,a2)==TRUE);
	makeAction(g,a2);
	//printf("Action a2 has been processed!");
	
	assert(getMostARCs(g) == UNI_A); //both UNI_A and UNI_B have 2 ARCs but UNI_A keeps points
	assert(getMostPublications(g) == NO_ONE);
	assert(getTurnNumber(g) == 1);
	assert(getWhoseTurn(g) == UNI_B);
	
	assert(getKPIpoints(g,UNI_B)==24);
	assert(getARCs(g,UNI_B)==2);
	assert(getGO8s(g,UNI_B)==0);
	assert(getCampuses(g,UNI_B)==2);
	assert(getIPs(g,UNI_B)==0);
	assert(getPublications(g,UNI_B)==0);
	assert(getStudents(g,UNI_B,STUDENT_THD)==0);
	assert(getStudents(g,UNI_B,STUDENT_BPS)==1);
	assert(getStudents(g,UNI_B,STUDENT_BQN)==1);
	assert(getStudents(g,UNI_B,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_B,STUDENT_MTV)==1);
	assert(getStudents(g,UNI_B,STUDENT_MMONEY)==1);
	
	//check UNI_A is not change
	assert(getKPIpoints(g,UNI_A)==34);
	assert(getARCs(g,UNI_A)==2);
	assert(getGO8s(g,UNI_A)==0);
	assert(getCampuses(g,UNI_A)==2);
	assert(getIPs(g,UNI_A)==0);
	assert(getPublications(g,UNI_A)==0);
	
	
	assert(getStudents(g,UNI_A,STUDENT_THD)==0);
	assert(getStudents(g,UNI_A,STUDENT_BPS)==1);
	assert(getStudents(g,UNI_A,STUDENT_BQN)==1);
	assert(getStudents(g,UNI_A,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_A,STUDENT_MTV)==1);
	assert(getStudents(g,UNI_A,STUDENT_MMONEY)==1);
	
	//check UNI_C is not change
	assert(getKPIpoints(g,UNI_C)==20);
	assert(getARCs(g,UNI_C)==0);
	assert(getGO8s(g,UNI_C)==0);
	assert(getCampuses(g,UNI_C)==2);
	assert(getIPs(g,UNI_C)==0);
	assert(getPublications(g,UNI_C)==0);
	
	assert(getStudents(g,UNI_C,STUDENT_THD)==0);
	assert(getStudents(g,UNI_C,STUDENT_BPS)==3);
	assert(getStudents(g,UNI_C,STUDENT_BQN)==3);
	assert(getStudents(g,UNI_C,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_C,STUDENT_MTV)==1);
	assert(getStudents(g,UNI_C,STUDENT_MMONEY)==1);

	a2.destination[0]='R';
	a2.destination[1]='R';
	a2.destination[2]='L';
	a2.destination[3]='R';
	a2.destination[4]='R';
	//a2.destination[5]='B';
	a2.destination[5]='\0';

	assert(isLegalAction(g,a2) == FALSE);
	//action a3;
	a3.actionCode=BUILD_CAMPUS;
	a3.destination[0]='R';
	a3.destination[1]='R';
	a3.destination[2]='L';
	a3.destination[3]='\0';
	makeAction(g,a3);
	
	assert(getMostARCs(g) == UNI_A); //both UNI_A and UNI_B have 2 ARCs but UNI_A keeps points
	assert(getMostPublications(g) == NO_ONE);
	assert(getTurnNumber(g) == 1);
	assert(getWhoseTurn(g) == UNI_B);
	
	assert(getKPIpoints(g,UNI_B)==34);
	assert(getARCs(g,UNI_B)==2);
	assert(getGO8s(g,UNI_B)==0);
	assert(getCampuses(g,UNI_B)==3);
	assert(getIPs(g,UNI_B)==0);
	assert(getPublications(g,UNI_B)==0);
	
	assert(getStudents(g,UNI_B,STUDENT_THD)==0);
	assert(getStudents(g,UNI_B,STUDENT_BPS)==0);
	assert(getStudents(g,UNI_B,STUDENT_BQN)==0);
	assert(getStudents(g,UNI_B,STUDENT_MJ)==0);
	assert(getStudents(g,UNI_B,STUDENT_MTV)==0);
	assert(getStudents(g,UNI_B,STUDENT_MMONEY)==1);
	
	//check UNI_A is not change
	assert(getKPIpoints(g,UNI_A)==34);
	assert(getARCs(g,UNI_A)==2);
	assert(getGO8s(g,UNI_A)==0);
	assert(getCampuses(g,UNI_A)==2);
	assert(getIPs(g,UNI_A)==0);
	assert(getPublications(g,UNI_A)==0);
	
	
	assert(getStudents(g,UNI_A,STUDENT_THD)==0);
	assert(getStudents(g,UNI_A,STUDENT_BPS)==1);
	assert(getStudents(g,UNI_A,STUDENT_BQN)==1);
	assert(getStudents(g,UNI_A,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_A,STUDENT_MTV)==1);
	assert(getStudents(g,UNI_A,STUDENT_MMONEY)==1);
	
	//check UNI_C is not change
	assert(getKPIpoints(g,UNI_C)==20);
	assert(getARCs(g,UNI_C)==0);
	assert(getGO8s(g,UNI_C)==0);
	assert(getCampuses(g,UNI_C)==2);
	assert(getIPs(g,UNI_C)==0);
	assert(getPublications(g,UNI_C)==0);
	
	assert(getStudents(g,UNI_C,STUDENT_THD)==0);
	assert(getStudents(g,UNI_C,STUDENT_BPS)==3);
	assert(getStudents(g,UNI_C,STUDENT_BQN)==3);
	assert(getStudents(g,UNI_C,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_C,STUDENT_MTV)==1);
	assert(getStudents(g,UNI_C,STUDENT_MMONEY)==1);

	throwDice(g,7);
	//action a1;
	a1.actionCode=OBTAIN_ARC;
	a1.destination[0]='L';
	a1.destination[1]='R';
	a1.destination[2]='L';
	a1.destination[3]='R';
	a1.destination[4]='L';
	a1.destination[5]='\0';
	makeAction(g,a1);
	
	assert(getMostARCs(g) == UNI_A); //both UNI_A and UNI_B have 2 ARCs but UNI_A keeps points
	assert(getMostPublications(g) == NO_ONE);
	assert(getTurnNumber(g) == 2);
	assert(getWhoseTurn(g) == UNI_C);
	
	assert(getKPIpoints(g,UNI_C)==22);
	assert(getARCs(g,UNI_C)==1);
	assert(getGO8s(g,UNI_C)==0);
	assert(getCampuses(g,UNI_C)==2);
	assert(getIPs(g,UNI_C)==0);
	assert(getPublications(g,UNI_C)==0);
	
	assert(getStudents(g,UNI_C,STUDENT_THD)==2);
	assert(getStudents(g,UNI_C,STUDENT_BPS)==2);
	assert(getStudents(g,UNI_C,STUDENT_BQN)==2);
	assert(getStudents(g,UNI_C,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_C,STUDENT_MTV)==0);
	assert(getStudents(g,UNI_C,STUDENT_MMONEY)==0);
	
	//check UNI_A all MTV and M$ students decide to switch to ThD's
	assert(getKPIpoints(g,UNI_A)==34);
	assert(getARCs(g,UNI_A)==2);
	assert(getGO8s(g,UNI_A)==0);
	assert(getCampuses(g,UNI_A)==2);
	assert(getIPs(g,UNI_A)==0);
	assert(getPublications(g,UNI_A)==0);
	
	
	assert(getStudents(g,UNI_A,STUDENT_THD)==2);
	assert(getStudents(g,UNI_A,STUDENT_BPS)==1);
	assert(getStudents(g,UNI_A,STUDENT_BQN)==1);
	assert(getStudents(g,UNI_A,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_A,STUDENT_MTV)==0);
	assert(getStudents(g,UNI_A,STUDENT_MMONEY)==0);

	//check UNI_B all MTV and M$ students decide to switch to ThD's
	assert(getKPIpoints(g,UNI_B)==34);
	assert(getARCs(g,UNI_B)==2);
	assert(getGO8s(g,UNI_B)==0);
	assert(getCampuses(g,UNI_B)==3);
	assert(getIPs(g,UNI_B)==0);
	assert(getPublications(g,UNI_B)==0);
	
	assert(getStudents(g,UNI_B,STUDENT_THD)==1);
	assert(getStudents(g,UNI_B,STUDENT_BPS)==0);
	assert(getStudents(g,UNI_B,STUDENT_BQN)==0);
	assert(getStudents(g,UNI_B,STUDENT_MJ)==0);
	assert(getStudents(g,UNI_B,STUDENT_MTV)==0);
	assert(getStudents(g,UNI_B,STUDENT_MMONEY)==0);



	a2.actionCode=OBTAIN_ARC;
	a2.destination[0]='L';
	a2.destination[1]='R';
	a2.destination[2]='L';
	a2.destination[3]='R';
	a2.destination[4]='\0';
	makeAction(g,a2);
	
	assert(getMostARCs(g) == UNI_A); //all UNIs have 2 ARCs but UNI_A keeps points
	assert(getMostPublications(g) == NO_ONE);
	assert(getTurnNumber(g) == 2);
	assert(getWhoseTurn(g) == UNI_C);
	
	assert(getKPIpoints(g,UNI_C)==24);
	assert(getARCs(g,UNI_C)==2);
	assert(getGO8s(g,UNI_C)==0);
	assert(getCampuses(g,UNI_C)==2);
	assert(getIPs(g,UNI_C)==0);
	assert(getPublications(g,UNI_C)==0);
	
	assert(getStudents(g,UNI_C,STUDENT_THD)==2);
	assert(getStudents(g,UNI_C,STUDENT_BPS)==1);
	assert(getStudents(g,UNI_C,STUDENT_BQN)==1);
	assert(getStudents(g,UNI_C,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_C,STUDENT_MTV)==0);
	assert(getStudents(g,UNI_C,STUDENT_MMONEY)==0);
	
	//check UNI_A is no change
	assert(getKPIpoints(g,UNI_A)==34);
	assert(getARCs(g,UNI_A)==2);
	assert(getGO8s(g,UNI_A)==0);
	assert(getCampuses(g,UNI_A)==2);
	assert(getIPs(g,UNI_A)==0);
	assert(getPublications(g,UNI_A)==0);
	
	
	assert(getStudents(g,UNI_A,STUDENT_THD)==2);
	assert(getStudents(g,UNI_A,STUDENT_BPS)==1);
	assert(getStudents(g,UNI_A,STUDENT_BQN)==1);
	assert(getStudents(g,UNI_A,STUDENT_MJ)==1);
	assert(getStudents(g,UNI_A,STUDENT_MTV)==0);
	assert(getStudents(g,UNI_A,STUDENT_MMONEY)==0);
	
	//check UNI_B is no change
	assert(getKPIpoints(g,UNI_B)==34);
	assert(getARCs(g,UNI_B)==2);
	assert(getGO8s(g,UNI_B)==0);
	assert(getCampuses(g,UNI_B)==3);
	assert(getIPs(g,UNI_B)==0);
	assert(getPublications(g,UNI_B)==0);
	
	assert(getStudents(g,UNI_B,STUDENT_THD)==1);
	assert(getStudents(g,UNI_B,STUDENT_BPS)==0);
	assert(getStudents(g,UNI_B,STUDENT_BQN)==0);
	assert(getStudents(g,UNI_B,STUDENT_MJ)==0);
	assert(getStudents(g,UNI_B,STUDENT_MTV)==0);
	assert(getStudents(g,UNI_B,STUDENT_MMONEY)==0);

	a2.destination[0]='L';
	a2.destination[1]='R';
	a2.destination[2]='L';
	a2.destination[3]='L';
	a2.destination[4]='\0';

	assert(isLegalAction(g,a2)==FALSE);
	
	//turn 0 is finished

	

	/*
	 * disposeGame to getMostArcs		   Paulus
	 * getMostPublications to getKPIpoints  Matt
	 * getArcs to end					   Sijia
	 */



	disposeGame(g);


	int failedDisciplines[] = {0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0};
	int failedDice[] = {2,6,12,6,7,8,9,3,5,6,7,3,7,2,4,5,11,10,9};
	g = newGame(failedDisciplines, failedDice);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	throwDice(g, 4);
	assert(getCampuses(g,UNI_A)==2);
	assert(getCampuses(g,UNI_B)==2);
	assert(getCampuses(g,UNI_C)==2);
	assert(getARCs(g,UNI_A)==0);
	assert(getARCs(g,UNI_B)==0);
	assert(getARCs(g,UNI_C)==0);

	disposeGame(g);

	//action a;
	int newFailDisciplines[] = {3,5,2,4,1,3,5,3,2,4,1,4,5,3,0,2,1,2,4};
	int newFailDice[] = {8,10,9,3,5,6,12,6,4,11,3,11,2,9,7,4,5,10,8};
	g = newGame(newFailDisciplines, newFailDice);
	throwDice(g, 11);
	a.actionCode = OBTAIN_ARC;
	strncpy(a.destination, "R", PATH_LIMIT - 1);
	a.destination[PATH_LIMIT - 1] = 0;
	a.disciplineFrom = 134537389, a.disciplineTo = -1075072952;
	makeAction(g, a);
	a.actionCode = OBTAIN_ARC;
	strncpy(a.destination, "RL", PATH_LIMIT - 1);
	a.destination[PATH_LIMIT - 1] = 0;
	a.disciplineFrom = 134537389, a.disciplineTo = -1075072952;
	makeAction(g, a);
	a.actionCode = BUILD_CAMPUS;
	strncpy(a.destination, "RL", PATH_LIMIT - 1);
	a.destination[PATH_LIMIT - 1] = 0;
	a.disciplineFrom = 134537389, a.disciplineTo = -1075072952;
	makeAction(g, a);
	throwDice(g, 11);
	a.actionCode = OBTAIN_PUBLICATION;
	strncpy(a.destination, "RL", PATH_LIMIT - 1);
	a.destination[PATH_LIMIT - 1] = 0;
	a.disciplineFrom = 134537389, a.disciplineTo = -1075072952;
	makeAction(g, a);
	throwDice(g, 12);
	a.actionCode = OBTAIN_PUBLICATION;
	strncpy(a.destination, "RL", PATH_LIMIT - 1);
	a.destination[PATH_LIMIT - 1] = 0;
	a.disciplineFrom = 134537389, a.disciplineTo = -1075072952;
	makeAction(g, a);

	disposeGame(g);

	g = newGame(disciplines,dice);
	throwDice(g, 11);
	throwDice(g, 9);
	throwDice(g, 7);
	throwDice(g, 5);
	//printf("num of students 1 = %d", getStudents(g,2,1));

	disposeGame(g);

	g = newGame(disciplines,dice);
	throwDice(g, 6);
	throwDice(g, 6);
	throwDice(g, 6);
	throwDice(g, 11);
	throwDice(g, 11);
	throwDice(g, 11);
	throwDice(g, 11);

	a.actionCode = OBTAIN_ARC;
	strncpy(a.destination, "R", PATH_LIMIT - 1);
	a.destination[PATH_LIMIT - 1] = 0;
	a.disciplineFrom = 134554156, a.disciplineTo = 18;
	makeAction(g, a);
	a.actionCode = OBTAIN_ARC;
	strncpy(a.destination, "RL", PATH_LIMIT - 1);
	a.destination[PATH_LIMIT - 1] = 0;
	a.disciplineFrom = 134554156, a.disciplineTo = 18;
	makeAction(g, a);
	a.actionCode = OBTAIN_ARC;
	strncpy(a.destination, "RLL", PATH_LIMIT - 1);
	a.destination[PATH_LIMIT - 1] = 0;
	a.disciplineFrom = 134554156, a.disciplineTo = 18;
	makeAction(g, a);
	a.actionCode = RETRAIN_STUDENTS;
	strncpy(a.destination, "RLLR", PATH_LIMIT - 1);
	a.destination[PATH_LIMIT - 1] = 0;
	a.disciplineFrom = 4, a.disciplineTo = 1;
	makeAction(g, a);


	disposeGame(g);




	int diceJabez[] = {
		1,1,1,
		1,1,1,1,
		3,1,1,1,4,
		1,1,1,1,
		1,1,1
	};
	g = newGame(disciplinesMTV,diceJabez);
	assert(getStudents(g,UNI_A,STUDENT_MTV)==1);
	throwDice(g,1);
	assert(getStudents(g,UNI_A,STUDENT_MTV)==1);
	throwDice(g,4);
	assert(getStudents(g,UNI_A,STUDENT_MTV)==2);
	throwDice(g,3);
	assert(getStudents(g,UNI_A,STUDENT_MTV)==3);
	disposeGame(g);

	g = newGame(disciplines,dice);
	assert(getStudents(g,UNI_A,STUDENT_MJ)==1);
	assert(getDiceValue(g,11)==6);
	assert(getDiscipline(g,11)==STUDENT_MJ);
	throwDice(g,6);
	assert(getStudents(g,UNI_A,STUDENT_MJ)==2);
	disposeGame(g);

	printf("All tests Passed!! WOOHOO!! \n");
}