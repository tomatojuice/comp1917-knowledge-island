/*Simple Skeleton of runGame
  Matthew Phillips
  Copied from the lecture in which Salil wrote some psuedo code (There are errors)
  Lecture 04/05/2015
  REMINDER<<<<<<<<<<<<<
  printBoard needs to be removed from here as the function violates abstraction
*/
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>
#include "Game.h"
#include "mechanicalTurk.h"
#define DEFAULT_DISCIPLINES {STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, \
        STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, \
        STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, \
        STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, \
        STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS}
#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

#define WINNING_KPI 150
#define MAX_2DICE_VAL 11

#define NO_WINNER 0
#define WINNER 1

int randomRollDice(void);
void printBoard(Game g);

#define MAIN1
//#define MAIN2


#ifdef MAIN1
int main(int argc, char *argv[]){

   int disciplines[] = DEFAULT_DISCIPLINES;
   int dice[] = DEFAULT_DICE;
   int status = NO_WINNER;
   Game g = newGame(disciplines, dice);
   
   int diceValue = randomRollDice(); //simulate throw of two dice
   assert(randomRollDice() >= 2 && randomRollDice() <=12);
   throwDice(g, diceValue);

   //printf ("Please enter the first move of the game\n The actionCodes are\n");
   //printf ("PASS 0\nBUILD_CAMPUS 1\nBUILD_GO8 2\nOBTAIN_ARC 3\nSTART_SPINOFF 4\nRETRAIN_STUDENTS 7\n");
   action move;

   while(status == NO_WINNER){
       

        printf ("PASS 0\nBUILD_CAMPUS 1\nBUILD_GO8 2\nOBTAIN_ARC 3\nSTART_SPINOFF 4\nRETRAIN_STUDENTS 7\n");
        printf("Player %d, your resources are THD: %d, BPS: %d, BQN: %d, MJ: %d, MTV: %d, MMONEY: %d \n", getWhoseTurn(g),getStudents(g, getWhoseTurn(g), STUDENT_THD), 
                getStudents(g, getWhoseTurn(g), STUDENT_BPS), getStudents(g, getWhoseTurn(g), STUDENT_BQN),getStudents(g, getWhoseTurn(g), STUDENT_MJ),
                getStudents(g, getWhoseTurn(g), STUDENT_MTV), getStudents(g, getWhoseTurn(g), STUDENT_MMONEY));

       move = decideAction(g); // mechanicalTurk.c
        
       /*while (move.actionCode != PASS){
           printf("please enter a New move\n");
           scanf("%d", &(move.actionCode));
            
           if(move.actionCode == BUILD_CAMPUS || move.actionCode == OBTAIN_ARC || move.actionCode == BUILD_GO8){
               printf("please enter a direction\n");
               gets(move.destination);
           }

           if(move.actionCode == START_SPINOFF){
                
               srand(time(NULL));
               int condition = rand()%3;
               if (condition == 0){
                   move.actionCode = OBTAIN_IP_PATENT; 
               }else{
                   move.actionCode = OBTAIN_PUBLICATION;
               }

           }

           if(move.actionCode == RETRAIN_STUDENTS){
              printf("THD 0\nBPS 1\n BQN 2\nMJ  3\nMTV 4\nMMONEY 5\n");
              printf("Enter discipline to retrain students from\t:");
              scanf("%d",&(move.disciplineFrom));
              printf("Enter discipline to retrain students to\t:");
              scanf("%d",&(move.disciplineTo));
           }
           */
           assert (isLegalAction(g,move) == TRUE);
           makeAction(g, move);

           //move = decideAction(g);

       

      if(getKPIpoints(g, getWhoseTurn(g)) >= WINNING_KPI){
       status = WINNER;

      }
      else {
      diceValue = randomRollDice(); //simulate throw of two dice
      throwDice(g, diceValue);
      }
   }
   
   printf("CONGRATULATIONS USER %d, YOU ARE THE WINNER\n",getWhoseTurn(g));
   disposeGame(g);

   return EXIT_SUCCESS;
}
#endif

#ifdef MAIN2
int main(int argc, char const *argv[])
{
    int disciplines[] = DEFAULT_DISCIPLINES;
   int dice[] = DEFAULT_DICE;

   Game g = newGame(disciplines, dice);

   printBoard(g);
  return EXIT_SUCCESS;
}
#endif

int randomRollDice(void){

	int diceValue;
  srand(time(NULL));
	diceValue = 2 + rand()%MAX_2DICE_VAL;
		
	return diceValue;

}