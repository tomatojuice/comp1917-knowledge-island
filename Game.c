 /*
 *  Game.c
 *	by 
 *	Jabez Wilson, 5027406
 *	log
 *	- more test and print functions required
 *
 *
 */
#include "Game.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <time.h>

//the values for UP, DOWN,LEFT, RIGHT are defined anticlockwise
#define UP 0
#define LEFT 1
#define DOWN 2
#define RIGHT 3

#define EVEN_ODD 1
#define ODD_EVEN 0

#define NUM_RESOURCES 6
#define STARTING_POINTS_PER_PLAYER 2
#define NUM_VERTICES_PER_REGION 6
#define DIRECTIONS 4
#define REGIONS_PER_VERTEX 3
#define NUM_VERTICES 54

#define VALUE_FOUND 1
#define VALUE_NOTFOUND 0

#define MAX_2DICE_VAL 11
#define NO_TRAINING 6

#define GO8_INDEX_OFFSET 3

#define LEGAL 1
#define NOT_LEGAL 0

#define MARK printf("MARKED \n");
//defaults
#define DEFAULT_DISCIPLINES {STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, \
				STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, \
				STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, \
				STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, \
				STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS}
#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

#define TRAINING_CENTRES {2,3,4,5,29,35,46,51,48,43}
#define TRAINING_DISCIPLINES {STUDENT_MTV,STUDENT_MTV,\
															STUDENT_MMONEY,STUDENT_MMONEY,\
														  STUDENT_BQN,STUDENT_BQN,\
														  STUDENT_MJ,STUDENT_MJ,\
														  STUDENT_BPS,STUDENT_BPS}
#define MAX_RETRAIN_CENTRES 10

#define GRID_ROW 11
#define GRID_COL 6			
#define UPPER 0
#define MIDDLE 1
#define LOWER 2					

#define EMPTY_V '|'
#define EMPTY_H	'-'
#define REP_A '+'
#define REP_B 'o'
#define REP_C 'x'						  

//PRINTS
//#define PRINT_REGION
//#define PRINT_LINKS

//TESTs

// #define TEST_PRINTGRID
// #define TEST_DISPOSEGRID 
// #define TEST_OPPOSITEDIR
 //#define TEST_GOTO


//  #define TEST_GETDISCIPLINE
//  #define TEST_GETDICEVALUE

//  #define TEST_GETCAMPUS
//  #define TEST_GETARC

//  #define TEST_GETMOSTARCS
//  #define TEST_GETMOSTPUBLICATIONS


// #define TEST_GETWHOSETURN
// #define TEST_GETTURNNUMBER
// #define TEST_THROWDICE
//  #define TEST_ISLEGALACTION


// #define TEST_NEWACTION


//LOGs
//#define REASON_ISLEGALACITON
//#define LOG_ISLEGALACTION			
//#define LOG_TESTISLEGALACTION											  
//when a main function is necassary uncomment

//for TESTs most likely
//#define RUN_MAIN

//######################################################################
//BEGINNING OF STRUCT DEFINITIONS
//######################################################################
	typedef struct _vertex *vertexPtr;
	typedef struct _region *regionPtr;
	typedef struct _player *playerPtr;
	
	typedef struct _vertex vertex;
	typedef struct _region region;
	typedef struct _player player;

	struct _vertex {
	 	//owner of this vertex or NULL
	 	playerPtr owner;
	 	//campus at this vertex or VACANT_VERTEX
	 	int campus;
	 	//vertex Id of this vertex
	 	int vertexID;
	 	//ARC grants corresponding with neighbour, see example
	 	int ARC[DIRECTIONS];
	    regionPtr regions[REGIONS_PER_VERTEX];
	 	//next pointers to neghboring vertices
	 	vertexPtr next, neighbour[DIRECTIONS];
	 	int training;
	};

	struct _player {
		int playerID;
		int KPI;
		int numARC;
		int numPatent;
		int numCampus;
		int numGO8;
		int numPublication;
		int resources[NUM_RESOURCES];
		int ratio[NUM_RESOURCES];//<<<<<<ratio is important
		vertexPtr start[STARTING_POINTS_PER_PLAYER];
	};

	struct _region {
		int regionID; 
		int requiredDiceVal;
		vertexPtr surrounding[NUM_VERTICES_PER_REGION];
		int discipline;
	};

	//actual game
	struct _game {
		vertexPtr origin;
		regionPtr regions;
		int whoseTurn;
		int diceValue;
		int numGO8;
		int turnNumber;
		playerPtr mostARC,mostPublication;
		playerPtr universities;
	};

//######################################################################
//BEGINNING OF STATIC FUNCTION DECLARATIONS
//######################################################################

	//prints the whole grid,
	//as a table having infoabout what each object in the grid points to
	//	static void printGrid(Game g);
	//	static void printRegionGrid(Game g);
	//	static void printPlayer(Game g);
	//returns pointer to vertex defined by path
	static vertexPtr goTo(vertexPtr origin,char * path,int* lastDir);
	//returns opposite direction
	//opppositeDir(UP) = DOWN
	static int oppositeDir(int i);
	//	static int randomRollDice ();

	//usage of new action
	//action move;
	//newAction(&move,BUILD_CAMPUS,"RLR",-1,-1);
	//newAction(&move,OBTAIN_ARC,"LRLR",0,0);
	//the last two can be any value as long as the code is not RETRAIN_STUDENTS
	//newAction(&move,RETRAIN_STUDENTS,"a",STUDENT_MJ,STUDENT_BPS);
	//destintion can be anything for RETRAIN_STUDENTS,OBTAIN_IP,OBTAIN_PUBLICATION
	//so when making the object, pass the address of the empty or an object whose values are not going to used
	//and after the function is called the passed object will have all the values you need
	//	static void newAction(action* move,int code,path destination,int disciplineFrom,int discipineTo);

	/*
		//test functions
		static void testOppositeDir();
		static void testGoTo();
		static void testGetDiscipline();
		static void testGetdiceValue();

	    static void testGetKPI();
	    static void testgetGO8s();
	    static void testGetIPs();
	    static void testGetPublications();
		static void testGetCampus();
		static void testGetARC();
	    static void testGetStudents();

		static void testRandomRollDice();
		static void testGetMostArcs();
		static void testGetMostPublications();

		static void testGetTurnNumber();
		static void testGetWhoseTurn();
		static void testIslegalAction();

		static void testThrowDice();
		static void testNewAction();
	*/
//		static void testGoTo();

	//if main is being used in this fiel then this function needs to declared
	//oterwise it will be declared in another file
	#ifdef RUN_MAIN
	//		void printBoard(Game g);
	#endif

//######################################################################
//BEGINNING OF MAIN
//######################################################################
  #ifdef RUN_MAIN
		int main(int argc, char *argv[]) {

			int disciplines[] = DEFAULT_DISCIPLINES;
	  		int dice[] = DEFAULT_DICE;
			Game g = newGame(disciplines,dice);
			
			#ifdef PRINT_LINKS
 //				printGrid(g);
			#endif

			#ifdef PRINT_REGION
				printRegionGrid(g);
			#endif

 //*
			#ifdef TEST_GOTO
				testGoTo();
				printf("GO TO PASSED\n");
			#endif
			#ifdef TEST_OPPOSITEDIR
				testOppositeDir();
				printf("OPPOSITE DIRECTION PASSED\n");
			#endif
			#ifdef TEST_GETDISCIPLINE
				testGetDiscipline();
				printf("Get discipline tests passed \n");
			#endif

			#ifdef TEST_GETDICEVALUE
				testGetdiceValue();
				printf("Get diceValue tests passed \n");
			#endif

			#ifdef TEST_GETCAMPUS
				testGetCampus();
				printf("Get campus tests passed\n");
			#endif
			#ifdef TEST_GETARC
				testGetARC();
				printf("Get ARC tests passed\n");
			#endif

			#ifdef TEST_GETKPI
			    testGetKPI();
			    printf("Get KPIs tests passed\n");
			#endif
			#ifdef TEST_GETGO8
			    testgetGO8s();
			    printf("Get GO8 tests passed\n");
			#endif
			#ifdef TEST_GETIP
			    testGetIPs();
			    printf("Get IPs tests passed\n");
			#endif
			#ifdef TEST_GETPUBLICATION
			    testGetPublications();
			    printf("Get Publication tests passed\n");
			#endif
			#ifdef TEST_GETSTUDENT
			    testGetStudents();
			    printf("Get Student tests passed\n");
			#endif
	        
			#ifdef TEST_GETMOSTARCS
				testGetMostArcs();
				printf("Get MostARCs tests passed\n");
			#endif
			#ifdef TEST_GETMOSTPUBLICATIONS
				testGetMostPublications();
				printf("Get getMostPublications passed\n");
			#endif

			#ifdef TEST_GETTURNNUMBER
				testGetTurnNumber();
				printf("Get Turn Number passed\n");
			#endif
			#ifdef TEST_GETWHOSETURN
				testGetWhoseTurn();
				printf("Get Whose Turn passed\n");
			#endif

			#ifdef TEST_THROWDICE
				testThrowDice();
				printf("Throw Dice passed\n");
			#endif

			#ifdef TEST_NEWACTION
				testNewAction();
				printf("New action Passed\n");
			#endif

			#ifdef TEST_ISLEGALACTION
				testIslegalAction();
				printf("isLegalAction passed\n");
			#endif

			//printBoard(g);

			printf("All Tests Passed!\n");
			//*/

			disposeGame(g);
			return EXIT_SUCCESS;
		}


	#endif

//######################################################################
//BEGINNING OF FUNCTION DEFINITIONS
//######################################################################

	action newAction(int a,path b,int c,int d){
		action ret;
		ret.actionCode = a;
		strcpy(ret.destination,b);
		ret.disciplineFrom = c;
		ret.disciplineTo = d;

		return ret;
	}

	Game newGame (int discipline[], int dice[]) {
		Game ret = malloc(sizeof(struct _game));//freed
		assert(ret!=NULL);

		vertexPtr vertexTmp = NULL,it = NULL;
		//regionPtr regionTmp = NULL;
		playerPtr playerTmp = NULL;
		int i = 0,j=0,k=0;
		int linkType;

		int trainingCentres[] = TRAINING_CENTRES;
		int trainingDisciplines[] = TRAINING_DISCIPLINES;

		//intitilaizing the primary values
		//i.e the ones which need not be objects or pointers to objects
			ret->whoseTurn = NO_ONE;
			ret->diceValue = -1;
			ret->turnNumber = -1;
			ret->numGO8 = 0;

			ret->mostARC = NULL;
			ret->mostPublication = NULL;

		ret->origin = malloc(sizeof(vertex)*NUM_VERTICES);//freed
		assert(ret->origin != NULL);

		//following while sets all the values of Origin array to default
		//and creates a network with *next
			it = ret->origin;
			i = 0;
			while(i<NUM_VERTICES) {
				it->owner = NO_ONE;
				it->campus = VACANT_VERTEX;
				it->vertexID = i;
				it->training = NO_TRAINING;
				it->ARC[0] = VACANT_ARC;
				it->ARC[1] = VACANT_ARC;
				it->ARC[2] = VACANT_ARC;
				it->ARC[3] = VACANT_ARC;

				it->regions[0] = NULL;
				it->regions[1] = NULL;
				it->regions[2] = NULL;

				it->neighbour[0] = NULL;
				it->neighbour[1] = NULL;
				it->neighbour[2] = NULL;
				it->neighbour[3] = NULL;
				if(i<NUM_VERTICES-1) {
					vertexTmp = it;
					it++;
					vertexTmp->next = it;
				}
				else{
					it->next = NULL;
				}
				i++;
			}

		//Initializing all links (neighbours)
			vertexTmp = ret->origin;
			//set links between vertices 0-5 48-53;
			//RIGHT LEFT
			i = 0;
			while(i<3) {
				vertexTmp[2*i].neighbour[RIGHT] = &vertexTmp[2*i+1];
				vertexTmp[48+ 2*i].neighbour[RIGHT] = &vertexTmp[48+ 2*i +1];
				vertexTmp[2*i+1].neighbour[LEFT] = &vertexTmp[2*i];
				vertexTmp[48+ 2*i+1].neighbour[LEFT] = &vertexTmp[48+ 2*i];
				i++;
			}

			i = 0;
			//UP DOWN
			while(i<2) {
				vertexTmp[i].neighbour[DOWN] = &vertexTmp[i+3];
				vertexTmp[i+3].neighbour[UP] = &vertexTmp[i];
				
				vertexTmp[49+ i].neighbour[DOWN] = &vertexTmp[49+i+3];
				vertexTmp[49+i+3].neighbour[UP] = &vertexTmp[49+i];
				i++;
			}

			//sets links between 3rd - 2nd and 9th - 10th
			i = 0;
			while(i<4) {
				vertexTmp[2+i].neighbour[DOWN] = &vertexTmp[7+i];
				vertexTmp[7+i].neighbour[UP] = &vertexTmp[2+i];

				vertexTmp[43+i].neighbour[DOWN] = &vertexTmp[48+i];
				vertexTmp[48+i].neighbour[UP] = &vertexTmp[43+i];
				i++;
			}

			//sets links within 3rd
			i = 0;
			while(i<3) {
				vertexTmp[6+ 2*i].neighbour[RIGHT] = &vertexTmp[6 + 2*i +1];
				vertexTmp[6+ 2*i +1].neighbour[LEFT] = &vertexTmp[6 + 2*i];

				vertexTmp[42+ 2*i].neighbour[RIGHT] = &vertexTmp[42+ 2*i +1];
				vertexTmp[42+ 2*i +1].neighbour[LEFT] = &vertexTmp[42 + 2*i];
				i++;
			}

			//sets relation of 3rd and 9th with others
			i = 6;
			while(i<12) {
				vertexTmp[i].neighbour[DOWN] = &vertexTmp[i+6];
				vertexTmp[i+36].neighbour[UP] = &vertexTmp[i-6+36];
				i++;
			}

			//sets links of all others only UP DOWN
			i = 12;
			while(i<42) {
				vertexTmp[i].neighbour[UP] = &vertexTmp[i-6];
				vertexTmp[i].neighbour[DOWN] = &vertexTmp[i+6];
				i++;
			}

			i = 12;
			linkType = ODD_EVEN;
			while(i<42) {
				if(linkType == ODD_EVEN) {
					if(i%2!=0) {
						vertexTmp[i].neighbour[RIGHT] = &vertexTmp[i+1];
						vertexTmp[i+1].neighbour[LEFT] = &vertexTmp[i];
					}
				}
				else {
					if(i%2==0) {
						vertexTmp[i].neighbour[RIGHT] = &vertexTmp[i+1];
						vertexTmp[i+1].neighbour[LEFT] = &vertexTmp[i];
					}
				}
				i++;
				if(i%6==0) {
					linkType = !linkType;
				}
			}

			//some extra link breaking as the above while creates extra links
			//namely 17-18, 29-30 41-42 these links extend over the boundary
			//i.e right edge connecting to left edge of board

			vertexTmp = ret->origin;
			vertexTmp[17].neighbour[RIGHT]=NULL;
			vertexTmp[29].neighbour[RIGHT]=NULL;
			vertexTmp[41].neighbour[RIGHT]=NULL;

			vertexTmp[18].neighbour[LEFT]=NULL;
			vertexTmp[30].neighbour[LEFT]=NULL;
			vertexTmp[42].neighbour[LEFT]=NULL;
			assert(ret!=NULL);
			#ifdef DEBUG_NEWGAME
				printf("ret!=NULL\n");
			#endif

		ret->regions = malloc(sizeof(region)*NUM_REGIONS);
		assert(ret->regions != NULL);
		//intiitalizing regions
			int startRegion[NUM_REGIONS]={
				6,18,30,2,13,25,37,0,8,20,32,44,4,15,27,39,10,22,34
			};
			i = 0;
			while(i<NUM_REGIONS) {

				ret->regions[i].regionID = i;
				ret->regions[i].requiredDiceVal = dice[i];
				ret->regions[i].discipline = discipline[i];

				it = &(ret->origin[startRegion[i]]);
				j=0;
				ret->regions[i].surrounding[j] = it;
				it = it->neighbour[RIGHT];j++;
				ret->regions[i].surrounding[j] = it;
				it = it->neighbour[DOWN];j++;
				ret->regions[i].surrounding[j] = it;
				it = it->neighbour[DOWN];j++;
				ret->regions[i].surrounding[j] = it;
				it = it->neighbour[LEFT];j++;
				ret->regions[i].surrounding[j] = it;
				it = it->neighbour[UP];j++;
				ret->regions[i].surrounding[j] = it;
				i++;
			}
			i = 0;
			while(i<NUM_REGIONS) {			
				//relation from vertex to the appropriate region
				j=0;
				while(j<NUM_VERTICES_PER_REGION) {
					it = ret->regions[i].surrounding[j];
					k=0;
					while(it->regions[k] != NULL && k< REGIONS_PER_VERTEX) {
						k++;
					}
					if(k==REGIONS_PER_VERTEX) {
						printf("i:%d j:%d k:%d\n",i,j,k);
						printf("vertex id %d \nregion id %d \nregion id %d \nregion id %d\n",it->vertexID,it->regions[0]->regionID,it->regions[1]->regionID,it->regions[2]->regionID);
					}
					assert(k!=REGIONS_PER_VERTEX);
					it->regions[k] = &(ret->regions[i]);
					j++;
				}
				
				i++;
			}

		ret->universities = malloc(sizeof(player)*NUM_UNIS);
		assert(ret->universities != NULL);
		//intiializing all player objects
			playerTmp = ret->universities;
			i=UNI_A;
			while(i<=UNI_C) {
				playerTmp->playerID = i;
				playerTmp->KPI = 20;
				playerTmp->numARC =0;
				playerTmp->numCampus = 2;
				playerTmp->numGO8 = 0;
				playerTmp->numPatent =0;
				playerTmp->numPublication =0;
				j=0;
				while(j<NUM_RESOURCES) {
					playerTmp->ratio[j] = 3;
					j++;
				}

				playerTmp->resources[STUDENT_BQN] = 3;
				playerTmp->resources[STUDENT_BPS] = 3;
				playerTmp->resources[STUDENT_MTV] = 1;
				playerTmp->resources[STUDENT_MJ] = 1;
				playerTmp->resources[STUDENT_MMONEY] = 1;
				playerTmp->resources[STUDENT_THD] = 0;

				playerTmp++;
				i++;
			}

		//intializing the starting unis for player
			//intializing the relations from university objects from game
			ret->universities[0].start[0] = &(ret->origin[0]);
			ret->universities[0].start[1] = &(ret->origin[53]);

			ret->universities[1].start[0] = &(ret->origin[12]);
			ret->universities[1].start[1] = &(ret->origin[41]);

			ret->universities[2].start[0] = &(ret->origin[42]);
			ret->universities[2].start[1] = &(ret->origin[11]);

			//intializing the relationg from vector objects
			ret->origin[0].owner = &(ret->universities[0]);
			ret->origin[53].owner = &(ret->universities[0]);

			ret->origin[12].owner = &(ret->universities[1]);
			ret->origin[41].owner = &(ret->universities[1]);

			ret->origin[42].owner = &(ret->universities[2]);
			ret->origin[11].owner = &(ret->universities[2]);

			ret->origin[0].campus = CAMPUS_A;
			ret->origin[53].campus = CAMPUS_A;

			ret->origin[12].campus = CAMPUS_B;
			ret->origin[41].campus = CAMPUS_B;

			ret->origin[42].campus = CAMPUS_C;
			ret->origin[11].campus = CAMPUS_C;
			
		//intializing the training centres
			i = 0;
			
			while(i<MAX_RETRAIN_CENTRES) {
				ret->origin[trainingCentres[i]].training = trainingDisciplines[i];
				i++;
			}
	
		//Final Return
			return ret;
	}

	void disposeGame (Game g) {
		free(g->origin);
		free(g->regions);
		free(g->universities);
		free(g);
	}

	static vertexPtr goTo(vertexPtr origin,char* path,int* lastDir) {
		
		vertexPtr traveller = NULL;
		char* pathDir = path;
		int right = FALSE,it = 0,last=DOWN;
		int tmp,state = VALUE_FOUND;

		int dir1,dir2,dir3;

		traveller = origin;

		//the first 'shift' is done at origin, 
		//this is straightforward as the relative right 'R' is the left neighbour when you look form above
		if(*pathDir == 'R') {
			//the direction of neighbour is opp of the pathDir
			traveller = traveller->neighbour[DOWN];
			last = DOWN;
			pathDir++;
		}
		else if(*pathDir == 'L'){
			traveller = traveller->neighbour[RIGHT];
			last = RIGHT;
			pathDir++;	
		}
		else if(*pathDir == 'B'){
			state = VALUE_NOTFOUND;
		}
		


		tmp = -1;
		//after the first shift it has to enter a while to 
		//get through the remaining path 
		while(*pathDir!='\0' && state == VALUE_FOUND) {
			state = VALUE_NOTFOUND;
			assert(*pathDir == 'R' || *pathDir == 'L' ||*pathDir == 'B' );
			//it is iterator and it will start from the opposite of last
			//as last was relative to the previous vertex 
			it = oppositeDir(last);
			right = FALSE;
			if(*pathDir == 'B') {
				tmp = oppositeDir(last);
				state = VALUE_FOUND;
			}
			else{
				it = (it+1)%4;
				if(traveller->neighbour[it]!=NULL){
					right = TRUE;
					if(*pathDir == 'R'){
						tmp = it;
						state = VALUE_FOUND;
					}
				}
				it = (it+1)%4;
				if(traveller->neighbour[it]!=NULL && state==VALUE_NOTFOUND){
					if(right == TRUE){
						//left
						if(*pathDir == 'L'){
							tmp = it;
							state = VALUE_FOUND;
						}
					}
					else {
						right = TRUE;
						if(*pathDir == 'R'){
							tmp = it;
							state = VALUE_FOUND;
						}
					}
				}
				it = (it+1)%4;
				if(traveller->neighbour[it]!=NULL && state==VALUE_NOTFOUND){
					//left
					if(*pathDir == 'L'){
						tmp = it;
						state = VALUE_FOUND;
					}
				}
				it = oppositeDir(last);
				dir1 = dir2 = dir3 = FALSE;
				if(traveller->neighbour[(it+1)%4]!=NULL)dir1 = TRUE;
				if(traveller->neighbour[(it+2)%4]!=NULL)dir2 = TRUE;
				if(traveller->neighbour[(it+3)%4]!=NULL)dir3 = TRUE;
				if(dir1 == FALSE && dir2 == TRUE && dir3==FALSE){
					if(traveller->neighbour[oppositeDir(last)]->neighbour[(last+3)%4]!=NULL){
						if(*pathDir == 'R'){
							tmp = last;
							state = VALUE_FOUND;
						}
					}
					else if(traveller->neighbour[oppositeDir(last)]->neighbour[(last+1)%4]!=NULL){
						if(*pathDir == 'L'){
							tmp = last;
							state = VALUE_FOUND;
						}	
					}
					else {
						printf("node ID : %d\n",traveller->vertexID);
						state = VALUE_NOTFOUND;
					}
				}
			}

			if(state == VALUE_FOUND){
				traveller = traveller->neighbour[tmp];
				last = tmp;
				pathDir++;
			}
			else{
				traveller = NULL;
			}
		}
		if(state == VALUE_NOTFOUND){
			traveller = NULL;
		}

		if(lastDir != NULL) {
			*lastDir = last;
		}

		return traveller;
	}

	static int oppositeDir(int i) {
		assert(i>=0&&i<4);
		int ret;
		ret = i+2;
		if(ret>=4) {
			ret-=4;
		}

		return ret;
	}

	int getCampus(Game g, path pathToVertex) {
		vertexPtr vertexTmp = goTo(g->origin,pathToVertex,NULL);
		if(vertexTmp == NULL){printf("INVALID PATH %s\n", pathToVertex);}
		assert(vertexTmp != NULL);
		int ret = vertexTmp->campus;
		return ret;
	}

	int getARC(Game g, path pathToEdge) {
		int lastDir,BackDir;
		vertexPtr vertexTmp = goTo(g->origin,pathToEdge,&lastDir);
		if(vertexTmp == NULL){printf("INVALID PATH %s\n", pathToEdge);}
		int ret = VACANT_ARC;
		BackDir = oppositeDir(lastDir);
		assert(vertexTmp != NULL);
		ret = vertexTmp->ARC[BackDir];
		//assert(vertexTmp->ARC[BackDir] == vertexTmp->neighbour[BackDir]->ARC[lastDir]);
		return ret;
	}

	int getDiscipline(Game g, int regionID){
    int retDiscipline;

		retDiscipline = g->regions[regionID].discipline; 

		return retDiscipline;
	}

	int getDiceValue (Game g, int regionID){
    	int retDiceValue;
    	retDiceValue = g->regions[regionID].requiredDiceVal;
    	return retDiceValue;
	}

/*
	static int randomRollDice () {
		int diceValue;
        srand(time(NULL));
		diceValue = 2 + rand()%MAX_2DICE_VAL;

	
		return diceValue;
	}
*/
	
	int getMostARCs (Game g) {
		int retPlayer = 0;
		if (g->mostARC==NULL) {
			retPlayer = NO_ONE;
		}else{
			retPlayer = g->mostARC[0].playerID;
		}
		return retPlayer;
	}

	int getMostPublications (Game g) {
		int retPlayer = 0;
		if (g->mostPublication==NULL) {
			retPlayer = NO_ONE;
		}else{
			retPlayer = g->mostPublication->playerID;
		}
		
		return retPlayer;
	}

	int getTurnNumber (Game g) {
		return g->turnNumber;
	}

	int getWhoseTurn (Game g) {
		return g->whoseTurn;
	}

	int isLegalAction(Game g, action a) {
		
		int ret = TRUE;
		int status = VALUE_NOTFOUND;
		int i,j;
		int tmp,playerID;
		vertexPtr vTmp = NULL;
		//order for required (resources)
		//THD BPS BQN MJ MTV MMONEY
		int required[NUM_RESOURCES];
		path pathTmp = "";
		playerID = getWhoseTurn(g);
		if(playerID == NO_ONE){
			ret = FALSE;
			status = VALUE_FOUND;
			#ifdef REASON_ISLEGALACITON
				printf("REASON: No one is playing\n");
			#endif
		}

		//check that action code is defined
			i=0;
			if(a.actionCode > RETRAIN_STUDENTS || a.actionCode<PASS) {
				ret = FALSE;
				status = VALUE_FOUND;
				#ifdef REASON_ISLEGALACITON
				printf("REASON: Invalid action Code\n");
			#endif
			}
			if(a.actionCode == OBTAIN_PUBLICATION || a.actionCode == OBTAIN_IP_PATENT){
				ret = FALSE;
				status = VALUE_FOUND;
				#ifdef REASON_ISLEGALACITON
				printf("REASON: ip or publication code\n");
			#endif
			}
		#ifdef LOG_ISLEGALACTION
			printf("defined action code\n");
			printf("%d NOT_FOUND=0\n",status);
		#endif
		//check for destination to be only R,B,L
			if((a.actionCode == BUILD_CAMPUS ||a.actionCode == OBTAIN_ARC ||a.actionCode == BUILD_GO8 )&&status == VALUE_NOTFOUND) {
				//first is not 'B'
				i=0;
				if(a.destination[i] == 'B') {
					ret = FALSE;
					status = VALUE_FOUND;
				}
				while(a.destination[i]!='\0' && status == VALUE_NOTFOUND) {
					if(a.destination[i] != 'R' && a.destination[i] != 'B' && a.destination[i] != 'L') {
						ret = FALSE;
						status = VALUE_FOUND;
						#ifdef REASON_ISLEGALACITON
				printf("REASON: Invalid path: %s\n",a.destination);
			#endif
					}
					i++;
				}
				
			}
		#ifdef LOG_ISLEGALACTION
			printf("only RBL \n");
			printf("%d NOT_FOUND=0\n",status);
		#endif
		//check that end vertex is VACANT for BUILD_CAMPUS
			if(a.actionCode == BUILD_CAMPUS && status == VALUE_NOTFOUND){
				if(getCampus(g,a.destination)!=VACANT_VERTEX){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
				printf("REASON: NOT VACANT\n");
			#endif
				}
			}
		#ifdef LOG_ISLEGALACTION
			printf("VACANT_VERTEX for BUILD_CAMPUS\n");
			printf("%d NOT_FOUND=0\n",status);
		#endif

		//check that path is defined (BUILD_CAMPUS,BUILD_GO8,OBTAIN_ARC)
			if((a.actionCode == OBTAIN_ARC||a.actionCode==BUILD_GO8||a.actionCode==BUILD_CAMPUS)&&status == VALUE_NOTFOUND){
				vTmp = goTo(g->origin,a.destination,NULL);
				if(vTmp == NULL){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
				printf("REASON: Invalid path : %s\n",a.destination);
			#endif
				}
			}
		//check adjacent vertex is VACANT for BUILD_CAMPUS
			if(a.actionCode == BUILD_CAMPUS && status == VALUE_NOTFOUND){
				vTmp = goTo(g->origin,a.destination,NULL);
				i = UP;
				while(i<=RIGHT){
					if(vTmp->neighbour[i]!=NULL){
						if(vTmp->neighbour[i]->campus != VACANT_VERTEX){
							ret = FALSE;
							status = VALUE_FOUND;
							#ifdef REASON_ISLEGALACITON
				printf("REASON: adjascent vertex is occupied\n");
			#endif
						}
					}
					i++;
				}
			}
		#ifdef LOG_ISLEGALACTION
			printf("adjascent VACANT_VERTEX for BUILD_CAMPUS\n");
			printf("%d NOT_FOUND=0\n",status);
		#endif
		//check adjascent arc owned by player for BUILD CAMPUS
			if(a.actionCode == BUILD_CAMPUS && status == VALUE_NOTFOUND){
				vTmp = goTo(g->origin,a.destination,NULL);
				i = UP;
				j = VALUE_NOTFOUND;
				while(i<=RIGHT){
					if(vTmp->neighbour[i]!=NULL){
						if(vTmp->ARC[i] == playerID){
							j=VALUE_FOUND;
						}
					}
					i++;
				}
				if(j==VALUE_NOTFOUND){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
				printf("REASON: adjascent arc is not owned\n");
			#endif
				}
			}			
		#ifdef LOG_ISLEGALACTION
			printf("adjascent arc owned by player\n");
			printf("%d NOT_FOUND=0\n",status);
		#endif
		//check that end vertex is owned by player for BUILD_GO8
			if(a.actionCode == BUILD_GO8 && status == VALUE_NOTFOUND){
				tmp = getCampus(g,a.destination);
				//UNI_A == CAMPUS_A ..
				if(tmp!=playerID){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
				printf("REASON: campus is not owned by player for GO8\n");
			#endif
				}
			}
		#ifdef LOG_ISLEGALACTION
			printf("end vartex owned by player\n");
			printf("%d NOT_FOUND=0\n",status);
		#endif
		//check that numGO8 does not exceed 8(for BUILD_GO8)
			if(a.actionCode == BUILD_GO8 && status == VALUE_NOTFOUND){
				if(g->numGO8>=8){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
				printf("REASON: 8 GO8 is already built\n");
			#endif
				}
			}
		//check that arc is not ""
			if(a.actionCode == OBTAIN_ARC && status == VALUE_NOTFOUND){
				if(a.destination[0] == '\0'){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
						printf("REASON: arc was \"\" \n");
					#endif
				}
			}
		//check that arc is empty(OBTAIN ARC)
			if(a.actionCode == OBTAIN_ARC && status == VALUE_NOTFOUND){
				tmp = getARC(g,a.destination);
				if(tmp != VACANT_ARC){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
				printf("REASON: NOT VACANT ARC\n");
			#endif
				}
			}
		#ifdef LOG_ISLEGALACTION
			printf("VACANT_ARC\n");
			printf("%d NOT_FOUND=0\n",status);
		#endif
		//check that adjascent ARCs or Vertices are owned by player(OBTAIN_ARC)
			if(a.actionCode == OBTAIN_ARC && status == VALUE_NOTFOUND){
				//checks ARCs around the end node of path
				strcpy(pathTmp,a.destination);
				vTmp = goTo(g->origin,pathTmp,NULL);

				i = UP;
				j = VALUE_NOTFOUND;
				while(i<=RIGHT){
					if(vTmp->neighbour[i]!=NULL){
						if(vTmp->ARC[i] == playerID){
							j=VALUE_FOUND;
						}
					}
					i++;
				}

				//checks the node before end node of path
				pathTmp[strlen(pathTmp)-1] = '\0';
				vTmp = goTo(g->origin,pathTmp,NULL);

				i = UP;
				while(i<=RIGHT){
					if(vTmp->neighbour[i]!=NULL){
						if(vTmp->ARC[i] == playerID){
							j=VALUE_FOUND;
						}
					}
					i++;
				}

				//check that nodes of the arc is owned by player
				strcpy(pathTmp,a.destination);
				tmp = strlen(a.destination);
				if(getCampus(g,pathTmp)==playerID){
					j=VALUE_FOUND;
				}
				pathTmp[tmp-1]='\0';
				if(getCampus(g,pathTmp)==playerID){
					j=VALUE_FOUND;
				}

				if(j!=VALUE_FOUND){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
				printf("REASON: no adjascent arc\n");
			#endif
				}
			}
		#ifdef LOG_ISLEGALACTION
			printf("owned arc\n");
			printf("%d NOT_FOUND=0\n",status);
		#endif

		//check that disciplineFrom and disciplineTo is defined(RETRAIN_STUDENTS)
			if(a.actionCode == RETRAIN_STUDENTS && status == VALUE_NOTFOUND &&(a.disciplineTo>STUDENT_MMONEY || a.disciplineTo < STUDENT_THD || a.disciplineFrom>STUDENT_MMONEY || a.disciplineFrom<=STUDENT_THD)){
				ret = FALSE;
				status = VALUE_FOUND;
				#ifdef REASON_ISLEGALACITON
				printf("REASON: REQUIREMENT\n");
			#endif
			}
		
		//requirement clauses
			//BUILD_CAMPUS
			if(a.actionCode == BUILD_CAMPUS && status == VALUE_NOTFOUND){
				required[0] = 0;
				required[1] = 1;
				required[2] = 1;
				required[3] = 1;
				required[4] = 1;
				required[5] = 0;
				i = VALUE_NOTFOUND;
				j=0;
				while(j<NUM_RESOURCES && i == VALUE_NOTFOUND){
					if(getStudents(g,playerID,j)<required[j]){
						i= VALUE_FOUND;
					}
					j++;
				}
				if(i==VALUE_FOUND){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
				printf("REASON: REQUIREMENT\n");
			#endif
				}
			}

			//BUILD_GO8
			if(a.actionCode == BUILD_GO8 && status == VALUE_NOTFOUND){
				required[0] = 0;
				required[1] = 0;
				required[2] = 0;
				required[3] = 2;
				required[4] = 0;
				required[5] = 3;
				i = VALUE_NOTFOUND;
				j=0;
				while(j<NUM_RESOURCES && i == VALUE_NOTFOUND){
					if(getStudents(g,playerID,j)<required[j]){
						i= VALUE_FOUND;
					}
					j++;
				}
				if(i==VALUE_FOUND){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
				printf("REASON: REQUIREMENT\n");
			#endif
				}
			}

			//OBTAIN_ARC
			if(a.actionCode == OBTAIN_ARC && status == VALUE_NOTFOUND){
				required[0] = 0;
				required[1] = 1;
				required[2] = 1;
				required[3] = 0;
				required[4] = 0;
				required[5] = 0;
				i = VALUE_NOTFOUND;
				j=0;
				while(j<NUM_RESOURCES && i == VALUE_NOTFOUND){
					if(getStudents(g,playerID,j)<required[j]){
						i= VALUE_FOUND;
					}
					j++;
				}
				if(i==VALUE_FOUND){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
				printf("REASON: REQUIREMENT\n");
			#endif
				}
			}

			//START_SPINOFF and others
			if((a.actionCode == OBTAIN_IP_PATENT || a.actionCode == OBTAIN_PUBLICATION || a.actionCode == START_SPINOFF) && status == VALUE_NOTFOUND){
				required[0] = 0;
				required[1] = 0;
				required[2] = 0;
				required[3] = 1;
				required[4] = 1;
				required[5] = 1;
				i = VALUE_NOTFOUND;
				j=0;
				while(j<NUM_RESOURCES && i == VALUE_NOTFOUND){
					if(getStudents(g,playerID,j)<required[j]){
						i= VALUE_FOUND;
					}
					j++;
				}
				if(i==VALUE_FOUND){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
				printf("REASON: REQUIREMENT\n");
			#endif
				}
			}

			if(a.actionCode == RETRAIN_STUDENTS && status == VALUE_NOTFOUND){
				if(getStudents(g,playerID,a.disciplineFrom)<getExchangeRate(g,playerID,a.disciplineFrom,a.disciplineTo)){
					ret = FALSE;
					status = VALUE_FOUND;
					#ifdef REASON_ISLEGALACITON
				printf("REASON: REQUIREMENT for retrain\n");
			#endif
				}
			}
		#ifdef LOG_ISLEGALACTION
			printf("requirement\n");
			printf("%d NOT_FOUND=0\n",status);

		#endif

		return ret;
	}

	void throwDice (Game g, int diceScore){
		int cycle = 0;
		int vertexCnt = 0;
		while (cycle < NUM_REGIONS){
			if (diceScore == getDiceValue(g, cycle)){
				vertexCnt = 0;
				while (vertexCnt < NUM_VERTICES_PER_REGION){
					if(g->regions[cycle].surrounding[vertexCnt]->owner != NULL){
						(g->regions[cycle].surrounding[vertexCnt]->owner->resources[getDiscipline(g,cycle)])++;
						if(g->regions[cycle].surrounding[vertexCnt]->campus == GO8_A){
							(g->regions[cycle].surrounding[vertexCnt]->owner->resources[getDiscipline(g,cycle)])++;
						}
					}
					vertexCnt++;
				} 
			}
			cycle++;
		}

		if (diceScore == 7){
			int playerID = UNI_A;
			playerPtr playerTmp = g->universities;
			while (playerID<=UNI_C){
				while (playerTmp->resources[STUDENT_MMONEY] != 0){
					playerTmp->resources[STUDENT_MMONEY]--;
					playerTmp->resources[STUDENT_THD]++;
				}
				while (playerTmp->resources[STUDENT_MTV] != 0){
					playerTmp->resources[STUDENT_MTV]--;
					playerTmp->resources[STUDENT_THD]++;
				}
				
				playerTmp++;
				playerID++;
			}
		}
		g->turnNumber++;
		g->whoseTurn = ((g->turnNumber)%3)+1;
		g->diceValue = diceScore;
	}

	// return the number of KPI points the specified player currently has
    int getKPIpoints (Game g, int player){
        return g->universities[player-1].KPI;
    }

	// return the number of ARC grants the specified player currently has
    int getARCs (Game g, int player){
        return g->universities[player-1].numARC;
    }

	// return the number of GO8 campuses the specified player currently has
    int getGO8s (Game g, int player){
        return g->universities[player-1].numGO8;
    }

	// return the number of normal Campuses the specified player currently has
    int getCampuses (Game g, int player){
        return g->universities[player-1].numCampus;
    }

	// return the number of IP Patents the specified player currently has
    int getIPs (Game g, int player){
        return g->universities[player-1].numPatent;
    }

	// return the number of Publications the specified player currently has
    int getPublications (Game g, int player){
        return g->universities[player-1].numPublication;
    }

	// return the number of students of the specified discipline type
	// the specified player currently has
    int getStudents (Game g, int player, int discipline){
        return g->universities[player-1].resources[discipline];
    }

	// return how many students of discipline type disciplineFrom
	// the specified player would need to retrain in order to get one
	// student of discipline type disciplineTo.
	int getExchangeRate (Game g, int player,int disciplineFrom, int disciplineTo){
		//assert(disciplineFrom != STUDENT_THD);
		return g->universities[player-1].ratio[disciplineFrom];
	}

//##########################################################################

	//##################################################################
	// BEGIN: PAULUS' FUNCTIONS
	//##################################################################

	// make the specified action for the current player and update the 
	// game state accordingly.  
	// The function may assume that the action requested is legal.
	// START_SPINOFF is not a legal action here
	void makeAction (Game g, action a) {
		if(a.actionCode != OBTAIN_IP_PATENT && a.actionCode != OBTAIN_PUBLICATION)
		assert(isLegalAction(g,a)==TRUE);
		else assert(isLegalAction(g,newAction(START_SPINOFF,"",0,0))==TRUE);

		playerPtr currentPlayer = &(g -> universities[getWhoseTurn(g) - 
			1]);



		if (a.actionCode == PASS) {

			// Do nothing

		} else if (a.actionCode == BUILD_CAMPUS) {

			vertexPtr v = goTo(g -> origin, a.destination, NULL);
			v -> campus = getWhoseTurn(g);
			//I edited this line to fix compilation error
			//-Jabez
			v -> owner = &(g->universities[getWhoseTurn(g)-1]);

			// decrease resources
			currentPlayer -> resources[STUDENT_BPS] --;
			currentPlayer -> resources[STUDENT_BQN] --;
			currentPlayer -> resources[STUDENT_MJ] --;
			currentPlayer -> resources[STUDENT_MTV] --;

			// increment numCampus
			currentPlayer -> numCampus++;

			// increase KPI
			currentPlayer -> KPI += 10;

			if(v->training != NO_TRAINING){
				currentPlayer -> ratio[v->training] = 2;
			}

		} else if (a.actionCode == BUILD_GO8) {

			vertexPtr v = goTo(g -> origin, a.destination, NULL);
			v -> campus = getWhoseTurn(g) + GO8_INDEX_OFFSET;

			// decrease resources
			currentPlayer -> resources[STUDENT_MJ] -= 2;
			currentPlayer -> resources[STUDENT_MMONEY] -= 3;

			//incCampus numGO8
			currentPlayer -> numGO8++;
			g->numGO8++;

			// increase KPI
			currentPlayer -> KPI += 20;
			currentPlayer -> KPI -= 10;

		} else if (a.actionCode == OBTAIN_ARC) {
			
			// Get ultimate vertex
			int forwardDir;
			vertexPtr end = goTo(g -> origin, a.destination,
				&forwardDir);

			// Get penultimate vertex
			int reverseDir = oppositeDir(forwardDir);
			vertexPtr start = end -> neighbour[reverseDir];

			// Set the forward ARC
			start -> ARC[forwardDir] = getWhoseTurn(g);

			// Set the reverse ARC
			end -> ARC[reverseDir] = getWhoseTurn(g);

			// decrease resources
			currentPlayer -> resources[STUDENT_BPS] --;
			currentPlayer -> resources[STUDENT_BQN] --;

			//inc numARC
			currentPlayer -> numARC++;

			// increase KPI
			currentPlayer -> KPI += 2;

			// Check if player has most ARCs
			// and increase KPI accordingly
			// this works even if the same player leads again
			int leadingARCs = NO_ONE;
			if (g -> mostARC == NULL) {
				if(g->universities[0].numARC == g->universities[1].numARC){
					leadingARCs = g->universities[0].numARC;
				}else if(g->universities[0].numARC == g->universities[2].numARC){
					leadingARCs = g->universities[0].numARC;
				}else if(g->universities[1].numARC == g->universities[2].numARC){
					leadingARCs = g->universities[1].numARC;
				}
			} else {
				leadingARCs = g -> mostARC -> numARC;
			}
			
			if ( (currentPlayer -> numARC) > leadingARCs ) {

				// Decrease KPI away from old leader
				if (g -> mostARC != NULL) {
					g -> mostARC -> KPI -= 10;
					g -> mostARC = NULL;
				}
				g -> mostARC = currentPlayer;

				// Increase KPI of new leader
				g -> mostARC -> KPI += 10;
				
			}

		} else if (a.actionCode == OBTAIN_PUBLICATION) {
			
			currentPlayer -> numPublication ++;

			// decrease resources
			currentPlayer -> resources[STUDENT_MJ] --;
			currentPlayer -> resources[STUDENT_MTV] --;
			currentPlayer -> resources[STUDENT_MMONEY] --;

			// Check if player has most publications
			// and increase KPI accordingly
			// this works even if the same player leads again
			int leadingPubs;
			if (g -> mostPublication == NULL) {
				leadingPubs = 0;
			} else {
				leadingPubs = g -> mostPublication -> numPublication;
			}
			
			if ( (currentPlayer -> numPublication) > leadingPubs) {

				// Decrease KPI away from old leader
				if (g -> mostPublication != NULL) {
					g -> mostPublication -> KPI -= 10;
				}

				g -> mostPublication = currentPlayer;

				// Increase KPI of new leader
				g -> mostPublication -> KPI += 10;

			}

		} else if (a.actionCode == OBTAIN_IP_PATENT) {
			
			currentPlayer -> numPatent ++;

			// decrease resources
			currentPlayer -> resources[STUDENT_MJ] --;
			currentPlayer -> resources[STUDENT_MTV] --;
			currentPlayer -> resources[STUDENT_MMONEY] --;

			// increase KPI
			currentPlayer -> KPI += 10;

		} else if (a.actionCode == RETRAIN_STUDENTS) {

			// decrease old "from" students
			currentPlayer -> resources[a.disciplineFrom] -=
					getExchangeRate(g,currentPlayer->playerID,a.disciplineFrom,a.disciplineTo);

			// increase new "to" students
			currentPlayer -> resources[a.disciplineTo] ++;
		}

	}

	//##################################################################
	// END: PAULUS' FUNCTIONS
	//##################################################################

/*
//######################################################################
//TESTS
//######################################################################

	static void newAction(action* move,int code,path destination,int disciplineFrom,int disciplineTo){
		move->actionCode = code;
		strcpy(move->destination,destination);
		move->disciplineFrom = disciplineFrom;
		move->disciplineTo = disciplineTo;
	}

	static void printGrid(Game g) {
		assert(g != NULL);
		#ifdef TEST_PRINTGRID
			printf("g!=NULL\n");
		#endif
		vertexPtr tmp = NULL;
		tmp = g->origin;
		assert(tmp !=  NULL);
		#ifdef TEST_PRINTGRID
			printf("tmp!=NULL\n");
		#endif

		int i = 0;
		printf("    ID|    UP|  DOWN| RIGHT|  LEFT|  NEXT|\n");
		

		while(i<54) {
			printf("%6d|",tmp->vertexID);
			if(tmp->neighbour[UP] != NULL)printf("%6d|",tmp->neighbour[UP]->vertexID);
			else printf("      |");
			if(tmp->neighbour[DOWN] != NULL)printf("%6d|",tmp->neighbour[DOWN]->vertexID);
			else printf("      |");
			if(tmp->neighbour[RIGHT] != NULL)printf("%6d|",tmp->neighbour[RIGHT]->vertexID);
			else printf("      |");
			if(tmp->neighbour[LEFT] != NULL)printf("%6d|",tmp->neighbour[LEFT]->vertexID);
			else printf("      |");
			if(tmp->next != NULL)printf("%6d|",tmp->next->vertexID);
			else printf("      |");
			printf("\n");
			i++;
			tmp++;

		}	
		printf("\n\n");
	}

	static void printRegionGrid(Game g) {
		printf("    ID|V1    |V2    |V3    |V4    |V5    |V6    |\n");
		regionPtr regionTmp = g->regions;
		vertexPtr vertexTmp = g->origin;
		int i =0,j;
		while(i<NUM_REGIONS) {
			printf("%6d|",regionTmp->regionID);
			j=0;
			while(j<6) {
				printf("%6d|",regionTmp->surrounding[j]->vertexID);
				j++;
			}
			printf("\n");
			regionTmp++;
			i++;
		}
		printf("\n\n");
		printf("    ID|     1|     2|     3|\n");
		
		i=0;
		while(i<54) {
			printf("%6d|",vertexTmp->vertexID);
			j =0;
			while(j<3) {
				if(vertexTmp->regions[j]!=NULL)
					printf("%6d|",vertexTmp->regions[j]->regionID);
				else
					printf("      ");
				j++;
			}
			i++;
			vertexTmp++;
			printf("\n");
		}
		printf("\n\n");
		printf("    ID| DISC | DICE |\n");
		for(i=0;i<NUM_REGIONS;i++) {
			printf("%6d|",g->regions[i].regionID);
			printf("%6d|",g->regions[i].discipline);
			printf("%6d|\n",g->regions[i].requiredDiceVal);
		}
	}

	*/
	void printBoard(Game g){
		int i,j,k,area;
		int arc,campus,training;
		int grid[GRID_ROW][GRID_COL];
		i=0;
		while(i<GRID_ROW){
			j=0;
			while(j<GRID_COL){
				grid[i][j]=0;
				j++;
			}
			i++;
		}
		grid[0][0] = -1;
		grid[0][1] = -1;
		grid[0][4] = -1;
		grid[0][5] = -1;

		grid[1][0] = -1;
		grid[1][5] = -1;


		grid[10][0] = -1;
		grid[10][1] = -1;
		grid[10][4] = -1;
		grid[10][5] = -1;

		grid[9][0] = -1;
		grid[9][5] = -1;

		i=0;k=0;
		while(i<GRID_ROW){
			j=0;
			while(j<GRID_COL){
				if(grid[i][j]!= -1){
					grid[i][j] = k++;
				}
				j++;
			}
			i++;
		}

		area = UPPER;
		i=0;
		while(i<GRID_ROW){
			j=0;
			while(j<GRID_COL){
				if(grid[i][j]!=-1){
					if(area==UPPER){
						arc = g->origin[grid[i][j]].ARC[UP];
						training = g->origin[grid[i][j]].training;
						if(training!=NO_TRAINING){
							printf("%d",training);
						}
						else {
							printf(" ");
						}
						if(arc == VACANT_ARC){
							if(g->origin[grid[i][j]].neighbour[UP]!=NULL)
							printf("%c", EMPTY_V);
							else
							printf(" ");
						}
						else if(arc == ARC_A){
							printf("%c", REP_A);
						}
						else if(arc == ARC_B){
							printf("%c", REP_B);
						}
						else {
							printf("%c", REP_C);
						}
						if(training!=NO_TRAINING){
							printf("%d",training);
						}
						else {
							printf(" ");
						}
					}
					else if(area==MIDDLE){
						arc = g->origin[grid[i][j]].ARC[LEFT];
						if(arc == VACANT_ARC){
							if(g->origin[grid[i][j]].neighbour[LEFT]!=NULL)
							printf("%c", EMPTY_H);
							else
							printf(" ");
						}
						else if(arc == ARC_A){
							printf("%c", REP_A);
						}
						else if(arc == ARC_B){
							printf("%c", REP_B);
						}
						else if(arc == ARC_C){
							printf("%c", REP_C);
						}

						campus = g->origin[grid[i][j]].campus;
						if(campus== NO_ONE){
							printf(" ");
						}
						else if(campus== CAMPUS_A){
							printf("A");
						}
						else if(campus== CAMPUS_B){
							printf("B");
						}
						else if(campus== CAMPUS_C){
							printf("C");
						}
						arc = g->origin[grid[i][j]].ARC[RIGHT];
						if(arc == VACANT_ARC){
							if(g->origin[grid[i][j]].neighbour[RIGHT]!=NULL)
							printf("%c", EMPTY_H);
							else
							printf(" ");
						}
						else if(arc == ARC_A){
							printf("%c", REP_A);
						}
						else if(arc == ARC_B){
							printf("%c", REP_B);
						}
						else if(arc == ARC_C){
							printf("%c", REP_C);
						}
					}
					else if(area==LOWER){
						arc = g->origin[grid[i][j]].ARC[DOWN];
						if(arc == VACANT_ARC){
							if(g->origin[grid[i][j]].neighbour[DOWN]!=NULL)
							printf(" %c ", EMPTY_V);
							else
							printf("   ");
						}
						else if(arc == ARC_A){
							printf(" %c ", REP_A);
						}
						else if(arc == ARC_B){
							printf(" %c ", REP_B);
						}
						else if(arc == ARC_C){
							printf(" %c ", REP_C);
						}
					}
				}
				else {
					printf("   ");
				}
				
				j++;	
			}
			area = (area+1)%3;
			printf("\n");
			if(area==UPPER)
			{i++;}
		}
	}
/*
	static void testOppositeDir() {
		assert(oppositeDir(UP)==DOWN);
		assert(oppositeDir(DOWN)==UP);
		assert(oppositeDir(LEFT)==RIGHT);
		assert(oppositeDir(RIGHT)==LEFT);
	}

	static void testGoTo() {

		int something;
  		int disciplines[] = DEFAULT_DISCIPLINES;
  		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);

		assert(goTo(g->origin,"LL",NULL)==NULL);
		assert(goTo(g->origin,"LR",NULL)->vertexID==4);
		
		assert(goTo(g->origin,"LLRR",NULL)==NULL);
		assert(goTo(g->origin,"LRLL",NULL)==NULL);
		assert(goTo(g->origin,"LRLR",NULL)->vertexID==10);
		assert(goTo(g->origin,"RRL",NULL)->vertexID == 7);
		assert(goTo(g->origin,"RRLRL",NULL)->vertexID == 12);
		assert(goTo(g->origin,"RB",NULL)->vertexID == 0);
		assert(goTo(g->origin,"RLRLRLRLRL",NULL)->vertexID == 52);
		assert(goTo(g->origin,"LRL",NULL)->vertexID == 5);
		assert(goTo(g->origin,"RLR",&something)->vertexID == 14);
		assert(something == DOWN);
		assert(goTo(g->origin,"LRL",&something)->vertexID == 5);
		assert(something == RIGHT);

		assert(goTo(g->origin,"L",NULL)->vertexID == 1);
		assert(goTo(g->origin,"R",NULL)->vertexID == 3);
		disposeGame(g);
	}

	static void testGetDiscipline() {
		int disciplines[] = DEFAULT_DISCIPLINES;
		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);
		int i = 0;
		while(i<NUM_REGIONS) {
			assert(getDiscipline(g,i) == disciplines[i]);
			i++;
		}
		disposeGame(g);
	}

	static void testGetdiceValue() {
		int disciplines[] = DEFAULT_DISCIPLINES;
		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);
		int i = 0;
		while(i<NUM_REGIONS) {
			assert(getDiceValue(g,i) == dice[i]);
			i++;
		}
		disposeGame(g);
	}

	static void testGetCampus() {
		int disciplines[] = DEFAULT_DISCIPLINES;
		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);

		assert(getCampus(g, "R") == VACANT_VERTEX);
		assert(getCampus(g, "L") == VACANT_VERTEX);
		
		assert(getCampus(g, "RRRRR") == CAMPUS_B);
		assert(getCampus(g, "RB") == CAMPUS_A);
		
		assert(getCampus(g, "RLRLRLRLRLL") == CAMPUS_A);
		assert(getCampus(g, "RLRLRLRLRLR") == CAMPUS_A);
		assert(getCampus(g, "LLLLL") == CAMPUS_C);

		//abstraction violated for TESTging purposes
		g->origin[8].campus = CAMPUS_A;
		assert(getCampus(g, "RL") == CAMPUS_A);

		disposeGame(g);

    g = newGame(disciplines,dice);
    
    int i = UNI_A;
    while(i<=UNI_C){
        assert(getKPIpoints(g,i)==0);
        i++;
    }
    throwDice(g,6);
    action a1;
    a1.actionCode=OBTAIN_ARC;
    a1.destination[0]='L';
    a1.destination[1]='\0';
    assert(isLegalAction(g,a1)==TRUE);
    makeAction(g,a1);
    assert(getCampuses(g,UNI_A)==2);
    assert(getCampuses(g,UNI_B)==2);
    assert(getCampuses(g,UNI_C)==2);

    action a2;
    a2.actionCode=OBTAIN_ARC;
    a2.destination[0]= 'L';
    a2.destination[1]= 'R';
    a2.destination[2]= '\0';
    makeAction(g,a2);
    
    assert(getCampuses(g,UNI_A)==2);
    assert(getCampuses(g,UNI_B)==2);
    assert(getCampuses(g,UNI_C)==2);

    action a3;
    a3.actionCode=BUILD_CAMPUS;
    a3.destination[0]='L';
    a3.destination[1]='R';
    a3.destination[2]='\0';
    makeAction(g,a3);
    
    assert(getCampuses(g,UNI_A)==3);
    assert(getCampuses(g,UNI_B)==2);
    assert(getCampuses(g,UNI_C)==2);
    disposeGame(g);
	}

	static void testGetARC() {
		int disciplines[] = DEFAULT_DISCIPLINES;
		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);
		assert(getARC(g,"R")==VACANT_ARC);
		assert(getARC(g,"L")==VACANT_ARC);
		assert(getARC(g,"RRR")==VACANT_ARC);
		assert(getARC(g,"LLL")==VACANT_ARC);
		assert(getARC(g,"RLR")==VACANT_ARC);
		assert(getARC(g,"LLLLLRR")==VACANT_ARC);
	
		//for TESTging purposes, abstraction is violated
		g->origin[10].ARC[RIGHT] = ARC_A;
		g->origin[11].ARC[LEFT] = ARC_B;
		//yes even standards are violated

		assert(getARC(g,"LLLLL")==ARC_B);
		assert(getARC(g,"LLLL")==VACANT_ARC);
		assert(getARC(g,"LLLLLL")==VACANT_ARC);
		assert(getARC(g,"LLLLLB")==ARC_A);

		disposeGame(g);

		g = newGame(disciplines,dice);
		
		int i = UNI_A;
		while(i<=UNI_C){
		    assert(getKPIpoints(g,i)==0);
		    i++;
		}
		throwDice(g,6);
		action a1;
		a1.actionCode=OBTAIN_ARC;
		a1.destination[0]='L';
		a1.destination[1]='\0';
		assert(isLegalAction(g,a1)==TRUE);
		makeAction(g,a1);
		
		assert(getARCs(g,UNI_A)==1);
		assert(getARCs(g,UNI_B)==0);
		assert(getARCs(g,UNI_C)==0);
		
		action a2;
		a2.actionCode=OBTAIN_ARC;
		a2.destination[0]= 'L';
		a2.destination[1]= 'R';
		a2.destination[2]= '\0';
		makeAction(g,a2);
		
		assert(getARCs(g,UNI_A)==2);
		assert(getARCs(g,UNI_B)==0);
		assert(getARCs(g,UNI_C)==0);
		
		action a3;
		a3.actionCode=BUILD_CAMPUS;
		a3.destination[0]='L';
		a3.destination[1]='R';
		a3.destination[2]='\0';
		makeAction(g,a3);
		
		assert(getARCs(g,UNI_A)==2);
		assert(getARCs(g,UNI_B)==0);
		assert(getARCs(g,UNI_C)==0);
		disposeGame(g);
	}

	static void testRandomRollDice() {
		assert(randomRollDice() >= 2 && randomRollDice() <=12);
		assert(randomRollDice() >= 2 && randomRollDice() <=12);
		assert(randomRollDice() >= 2 && randomRollDice() <=12);
		assert(randomRollDice() >= 2 && randomRollDice() <=12);
		assert(randomRollDice() >= 2 && randomRollDice() <=12);
		assert(randomRollDice() >= 2 && randomRollDice() <=12);
		assert(randomRollDice() >= 2 && randomRollDice() <=12);
		assert(randomRollDice() >= 2 && randomRollDice() <=12);
		assert(randomRollDice() >= 2 && randomRollDice() <=12);
		assert(randomRollDice() >= 2 && randomRollDice() <=12);
		assert(randomRollDice() >= 2 && randomRollDice() <=12);
		assert(randomRollDice() >= 2 && randomRollDice() <=12);
	}

	static void testGetMostArcs() {
		int disciplines[] = DEFAULT_DISCIPLINES;
		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);
		assert (g != NULL);

		assert (getMostARCs(g) == NO_ONE);

		//Can't do any more tests until the makeAction/throwDice functions are written
		
		disposeGame(g);
	}

	static void testGetMostPublications() {
		int disciplines[] = DEFAULT_DISCIPLINES;
		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);
		assert (g != NULL);

		assert (getMostPublications(g) == NO_ONE);

		//Can't do any more tests until the makeAction/throwDice functions are written
		
		disposeGame(g);
	}

	static void testGetTurnNumber() {
		int disciplines[] = DEFAULT_DISCIPLINES;
		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);
		
		assert(getTurnNumber(g) == -1);

		disposeGame(g);
	}

	static void testGetWhoseTurn() {
		int disciplines[] = DEFAULT_DISCIPLINES;
		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);

		assert(getWhoseTurn(g) == NO_ONE);

		disposeGame(g);
	}


	static void testThrowDice(){
		int disciplines[] = DEFAULT_DISCIPLINES;
		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);

		int diceValue = 9;

		throwDice(g, diceValue);
		assert (getStudents(g, UNI_B, STUDENT_BQN) == 4);
		assert (getTurnNumber(g) == 0);
		assert (getWhoseTurn(g) == UNI_A);

		throwDice(g, diceValue);
		assert (getStudents(g, UNI_B, STUDENT_BQN) == 5);
		assert (getTurnNumber(g) == 1);
		assert (getWhoseTurn(g) == UNI_B);

		throwDice(g, diceValue);
		assert (getStudents(g, UNI_B, STUDENT_BQN) == 6);
		assert (getTurnNumber(g) == 2);
		assert (getWhoseTurn(g) == UNI_C);

		throwDice(g, diceValue);
		assert (getStudents(g, UNI_B, STUDENT_BQN) == 7);
		assert (getTurnNumber(g) == 3);
		assert (getWhoseTurn(g) == UNI_A);

		throwDice(g, diceValue);
		assert (getStudents(g, UNI_B, STUDENT_BQN) == 8);
		assert (getTurnNumber(g) == 4);
		assert (getWhoseTurn(g) == UNI_B);

		throwDice(g, diceValue);
		assert (getStudents(g, UNI_B, STUDENT_BQN) == 9);
		assert (getTurnNumber(g) == 5);
		assert (getWhoseTurn(g) == UNI_C);
        
        diceValue = 8;

        throwDice(g, diceValue);
		assert (getStudents(g, UNI_B, STUDENT_BQN) == 9);
		assert (getStudents(g, UNI_C, STUDENT_MJ) == 2);
		assert (getTurnNumber(g) == 6);
		assert (getWhoseTurn(g) == UNI_A);

		diceValue = 7;

		throwDice(g, diceValue);
		assert(getStudents(g, UNI_A, STUDENT_MMONEY) == 0);
		assert(getStudents(g, UNI_A, STUDENT_MTV) == 0);
		assert(getStudents(g, UNI_B, STUDENT_MMONEY) == 0);
		assert(getStudents(g, UNI_B, STUDENT_MTV) == 0);
		assert(getStudents(g, UNI_C, STUDENT_MMONEY) == 0);
		assert(getStudents(g, UNI_C, STUDENT_MTV) == 0);
		assert(getStudents(g, UNI_A, STUDENT_THD) == 2);

		disposeGame(g);

		g = newGame(disciplines,dice);
		g->origin[0].campus = GO8_A;
		assert(getStudents(g,UNI_A,STUDENT_MTV)==1);
		throwDice(g,11);
		assert(getStudents(g,UNI_A,STUDENT_MTV)==3);
		
		disposeGame(g);
	}

	static void testGetKPI(){
		int disciplines[] = DEFAULT_DISCIPLINES;
		int dice[] = DEFAULT_DICE;
		Game g = newGame(disciplines,dice);
		
		int i = UNI_A;
		while(i<=UNI_C){
		    assert(getKPIpoints(g,i)==0);
		    i++;
		}
		
		throwDice(g,6);
		action a1;
		a1.actionCode=OBTAIN_ARC;
		a1.destination[0]='L';
		a1.destination[1]='\0';
		assert(isLegalAction(g,a1)==TRUE);
		makeAction(g,a1);
		
		assert(getKPIpoints(g,UNI_A)==12);
		assert(getKPIpoints(g,UNI_B)==0);
		assert(getKPIpoints(g,UNI_C)==0);
		
		action a2;
		a2.actionCode=OBTAIN_ARC;
		a2.destination[0]= 'L';
		a2.destination[1]= 'R';
		a2.destination[2]= '\0';
		makeAction(g,a2);
		
		assert(getKPIpoints(g,UNI_A)==14);
		assert(getKPIpoints(g,UNI_B)==0);
		assert(getKPIpoints(g,UNI_C)==0);
		
		action a3;
		a3.actionCode=BUILD_CAMPUS;
		a3.destination[0]='L';
		a3.destination[1]='R';
		a3.destination[2]='\0';
		makeAction(g,a3);
		
		
		assert(getKPIpoints(g,UNI_A)==24);
		assert(getKPIpoints(g,UNI_B)==0);
		assert(getKPIpoints(g,UNI_C)==0);
	}

	static void testgetGO8s(){
	    int disciplines[] = DEFAULT_DISCIPLINES;
	    int dice[] = DEFAULT_DICE;
	    Game g = newGame(disciplines,dice);
	    
	    int i = UNI_A;
		    while(i<=UNI_C){
		        assert(getKPIpoints(g,i)==0);
		        i++;
		    }
	    throwDice(g,6);
	    action a1;
	    a1.actionCode=OBTAIN_ARC;
	    a1.destination[0]='L';
	    a1.destination[1]='\0';
	    assert(isLegalAction(g,a1)==TRUE);
	    makeAction(g,a1);
	    
	    assert(getGO8s(g,UNI_A)==0);
	    assert(getGO8s(g,UNI_B)==0);
	    assert(getGO8s(g,UNI_C)==0);
	    
	    action a2;
	    a2.actionCode=OBTAIN_ARC;
	    a2.destination[0]= 'L';
	    a2.destination[1]= 'R';
	    a2.destination[2]= '\0';
	    makeAction(g,a2);
	    
	    assert(getGO8s(g,UNI_A)==0);
	    assert(getGO8s(g,UNI_B)==0);
	    assert(getGO8s(g,UNI_C)==0);

	    action a3;
	    a3.actionCode=BUILD_CAMPUS;
	    a3.destination[0]='L';
	    a3.destination[1]='R';
	    a3.destination[2]='\0';
	    makeAction(g,a3);
	    
	    assert(getGO8s(g,UNI_A)==0);
	    assert(getGO8s(g,UNI_B)==0);
	    assert(getGO8s(g,UNI_C)==0);
	    disposeGame(g);
	}

	static void testGetIPs(){
	    int disciplines[] = DEFAULT_DISCIPLINES;
	    int dice[] = DEFAULT_DICE;
	    Game g = newGame(disciplines,dice);
	    
	    int i = UNI_A;
		    while(i<=UNI_C){
		        assert(getKPIpoints(g,i)==0);
		        i++;
		    }
	    throwDice(g,6);
	    action a1;
	    a1.actionCode=OBTAIN_ARC;
	    a1.destination[0]='L';
	    a1.destination[1]='\0';
	    assert(isLegalAction(g,a1)==TRUE);
	    makeAction(g,a1);
	    
	    assert(getIPs(g,UNI_A)==0);
	    assert(getIPs(g,UNI_B)==0);
	    assert(getIPs(g,UNI_C)==0);
	    
	    action a2;
	    a2.actionCode=OBTAIN_ARC;
	    a2.destination[0]= 'L';
	    a2.destination[1]= 'R';
	    a2.destination[2]= '\0';
	    makeAction(g,a2);
	    
	    assert(getIPs(g,UNI_A)==0);
	    assert(getIPs(g,UNI_B)==0);
	    assert(getIPs(g,UNI_C)==0);
	    
	    action a3;
	    a3.actionCode=BUILD_CAMPUS;
	    a3.destination[0]='L';
	    a3.destination[1]='R';
	    a3.destination[2]='\0';
	    makeAction(g,a3);
	    
	    assert(getIPs(g,UNI_A)==0);
	    assert(getIPs(g,UNI_B)==0);
	    assert(getIPs(g,UNI_C)==0);
	    disposeGame(g);
	}

	static void testGetPublications(){
	    int disciplines[] = DEFAULT_DISCIPLINES;
	    int dice[] = DEFAULT_DICE;
	    Game g = newGame(disciplines,dice);
	    
	    int i = UNI_A;
		    while(i<=UNI_C){
		        assert(getKPIpoints(g,i)==0);
		        i++;
		    }
	    throwDice(g,6);
	    action a1;
	    a1.actionCode=OBTAIN_ARC;
	    a1.destination[0]='L';
	    a1.destination[1]='\0';
	    assert(isLegalAction(g,a1)==TRUE);
	    makeAction(g,a1);
	    
	    assert(getPublications(g,UNI_A)==0);
	    assert(getPublications(g,UNI_B)==0);
	    assert(getPublications(g,UNI_C)==0);
	    
	    action a2;
	    a2.actionCode=OBTAIN_ARC;
	    a2.destination[0]= 'L';
	    a2.destination[1]= 'R';
	    a2.destination[2]= '\0';
	    makeAction(g,a2);
	    
	    assert(getPublications(g,UNI_A)==0);
	    assert(getPublications(g,UNI_B)==0);
	    assert(getPublications(g,UNI_C)==0);
	    
	    action a3;
	    a3.actionCode=BUILD_CAMPUS;
	    a3.destination[0]='L';
	    a3.destination[1]='R';
	    a3.destination[2]='\0';
	    makeAction(g,a3);
	    
	    assert(getPublications(g,UNI_A)==0);
	    assert(getPublications(g,UNI_B)==0);
	    assert(getPublications(g,UNI_C)==0);
	    disposeGame(g);
	}

	static void testGetStudents(){
	    int disciplines[] = DEFAULT_DISCIPLINES;
	    int dice[] = DEFAULT_DICE;
	    Game g = newGame(disciplines,dice);
	    
	    int i = UNI_A;
	    while(i<=UNI_C){
	        assert(getKPIpoints(g,i)==0);
	        i++;
	    }
	    throwDice(g,6);
	    action a1;
	    a1.actionCode=OBTAIN_ARC;
	    a1.destination[0]='L';
	    a1.destination[1]='\0';
	    assert(isLegalAction(g,a1)==TRUE);
	    makeAction(g,a1);
	    
	    assert(getStudents(g,UNI_A,STUDENT_THD)==0);
	    assert(getStudents(g,UNI_A,STUDENT_BPS)==2);
	    assert(getStudents(g,UNI_A,STUDENT_BQN)==2);
	    assert(getStudents(g,UNI_A,STUDENT_MJ)==2);
	    assert(getStudents(g,UNI_A,STUDENT_MTV)==1);
	    assert(getStudents(g,UNI_A,STUDENT_MMONEY)==1);
	    
	    i=UNI_B;
	    while(i<=UNI_C){
	        assert(getStudents(g,i,STUDENT_THD)==0);
	        assert(getStudents(g,i,STUDENT_BPS)==3);
	        assert(getStudents(g,i,STUDENT_BQN)==3);
	        assert(getStudents(g,i,STUDENT_MJ)==1);
	        assert(getStudents(g,i,STUDENT_MTV)==1);
	        assert(getStudents(g,i,STUDENT_MMONEY)==1);
	        i++;
	    }
	    
	    action a2;
	    a2.actionCode=OBTAIN_ARC;
	    a2.destination[0]= 'L';
	    a2.destination[1]= 'R';
	    a2.destination[2]= '\0';
	    makeAction(g,a2);
	    
	    assert(getStudents(g,UNI_A,STUDENT_THD)==0);
	    assert(getStudents(g,UNI_A,STUDENT_BPS)==1);
	    assert(getStudents(g,UNI_A,STUDENT_BQN)==1);
	    assert(getStudents(g,UNI_A,STUDENT_MJ)==2);
	    assert(getStudents(g,UNI_A,STUDENT_MTV)==1);
	    assert(getStudents(g,UNI_A,STUDENT_MMONEY)==1);
	    
	    
	    i=UNI_B;
	    while(i<=UNI_C){
	        assert(getStudents(g,i,STUDENT_THD)==0);
	        assert(getStudents(g,i,STUDENT_BPS)==3);
	        assert(getStudents(g,i,STUDENT_BQN)==3);
	        assert(getStudents(g,i,STUDENT_MJ)==1);
	        assert(getStudents(g,i,STUDENT_MTV)==1);
	        assert(getStudents(g,i,STUDENT_MMONEY)==1);
	        i++;
	    }
	    
	    action a3;
	    a3.actionCode=BUILD_CAMPUS;
	    a3.destination[0]='L';
	    a3.destination[1]='R';
	    a3.destination[2]='\0';
	    makeAction(g,a3);
	    
	    assert(getStudents(g,UNI_A,STUDENT_THD)==0);
	    assert(getStudents(g,UNI_A,STUDENT_BPS)==0);
	    assert(getStudents(g,UNI_A,STUDENT_BQN)==0);
	    assert(getStudents(g,UNI_A,STUDENT_MJ)==1);
	    assert(getStudents(g,UNI_A,STUDENT_MTV)==0);
	    assert(getStudents(g,UNI_A,STUDENT_MMONEY)==1);

	    i=UNI_B;
	    while(i<=UNI_C){
	        assert(getStudents(g,i,STUDENT_THD)==0);
	        assert(getStudents(g,i,STUDENT_BPS)==3);
	        assert(getStudents(g,i,STUDENT_BQN)==3);
	        assert(getStudents(g,i,STUDENT_MJ)==1);
	        assert(getStudents(g,i,STUDENT_MTV)==1);
	        assert(getStudents(g,i,STUDENT_MMONEY)==1);
	        i++;
	    }
	    disposeGame(g);
	}

	static void testNewAction(){
		action tmp;
		newAction(&tmp,3,"abc",1,2);
		assert(tmp.actionCode==3);
		assert(strcmp(tmp.destination,"abc")==0);
		assert(tmp.disciplineFrom==1);
		assert(tmp.disciplineTo==2);
	}

	static void testIslegalAction(){
	    int disciplines[] = DEFAULT_DISCIPLINES;
	    int dice[] = DEFAULT_DICE;
	    int i;
	    Game g = newGame(disciplines,dice);

	    action tmp;
	    g->turnNumber = 0;
	    g->whoseTurn = UNI_A;	
	    newAction(&tmp,BUILD_CAMPUS,"R",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
	    #ifdef LOG_TESTISLEGALACTION
				printf("BUILD_CAMPUS R\n");
			#endif
	    newAction(&tmp,BUILD_CAMPUS,"L",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
	    #ifdef LOG_TESTISLEGALACTION
				printf("BUILD_CAMPUS L\n");
			#endif
	    newAction(&tmp,OBTAIN_ARC,"L",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);
	    #ifdef LOG_TESTISLEGALACTION
				printf("OBTAIN_ARC L\n");
			#endif
	    newAction(&tmp,OBTAIN_ARC,"R",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);
	    #ifdef LOG_TESTISLEGALACTION
				printf("OBTAIN_ARC R\n");
			#endif
			newAction(&tmp,BUILD_CAMPUS,"RB",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
	    #ifdef LOG_TESTISLEGALACTION
				printf("BUILD_CAMPUS RB\n");
			#endif
	    newAction(&tmp,BUILD_CAMPUS,"LB",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
	    #ifdef LOG_TESTISLEGALACTION
				printf("BUILD_CAMPUS LB\n");
			#endif
	    newAction(&tmp,OBTAIN_ARC,"LB",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);
	    #ifdef LOG_TESTISLEGALACTION
				printf("OBTAIN_ARC R\n");
			#endif
	    newAction(&tmp,OBTAIN_ARC,"RB",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);
	    #ifdef LOG_TESTISLEGALACTION
				printf("OBTAIN_ARC RB\n");
			#endif


	    newAction(&tmp,OBTAIN_ARC,"RBAD",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);

			newAction(&tmp,START_SPINOFF,"RB",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
	    i =0;
	    while(i<NUM_RESOURCES){
	    	g->universities[UNI_A-1].resources[i] = 0;
	    	i++;
	    }


			newAction(&tmp,OBTAIN_IP_PATENT,"LR",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
			newAction(&tmp,OBTAIN_IP_PATENT,"L",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
			newAction(&tmp,OBTAIN_PUBLICATION,"abc",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
			newAction(&tmp,OBTAIN_PUBLICATION,"sf",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
	    //set some fake arcs
	    g->origin[1].ARC[DOWN] = ARC_A;
	    g->origin[1].neighbour[DOWN]->ARC[UP] = ARC_A;
	    g->origin[0].ARC[RIGHT] = ARC_A;
	    g->origin[0].neighbour[RIGHT]->ARC[LEFT] = ARC_A;
	    assert(g->origin[4].ARC[UP]==ARC_A);
	    g->origin[4].ARC[DOWN] = ARC_A;
	    g->origin[4].neighbour[DOWN]->ARC[UP] = ARC_A;
	    assert(g->origin[9].ARC[UP]==ARC_A);
	    g->origin[9].ARC[DOWN] = ARC_A;
	    g->origin[9].neighbour[DOWN]->ARC[UP] = ARC_A;
			g->origin[4].ARC[RIGHT] = ARC_A;
	    g->origin[4].neighbour[RIGHT]->ARC[LEFT] = ARC_A;

			newAction(&tmp,BUILD_CAMPUS,"LR",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
	    newAction(&tmp,OBTAIN_ARC,"LLR",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
	    newAction(&tmp,BUILD_CAMPUS,"LL",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
	    newAction(&tmp,OBTAIN_IP_PATENT,"LR",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
	    newAction(&tmp,OBTAIN_PUBLICATION,"LL",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);

	    i =0;
	    while(i<NUM_RESOURCES){
	    	g->universities[UNI_A-1].resources[i] = 10;
	    	i++;
	    }

			newAction(&tmp,OBTAIN_IP_PATENT,"LR",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);
			newAction(&tmp,OBTAIN_IP_PATENT,"L",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);
			newAction(&tmp,OBTAIN_PUBLICATION,"abc",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);
			newAction(&tmp,OBTAIN_PUBLICATION,"sf",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);

			newAction(&tmp,BUILD_CAMPUS,"LR",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);
	    newAction(&tmp,OBTAIN_ARC,"L",0,0);
	    assert(isLegalAction(g,tmp)== FALSE);
	    newAction(&tmp,BUILD_CAMPUS,"LL",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);
	    newAction(&tmp,OBTAIN_IP_PATENT,"LR",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);
	    newAction(&tmp,OBTAIN_PUBLICATION,"LL",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);
	    #ifdef LOG_TESTISLEGALACTION
	    	printf("checking GO8 tests\n");
	    #endif
			newAction(&tmp,BUILD_GO8,"",0,0);
	    assert(isLegalAction(g,tmp)==TRUE);
	    #ifdef LOG_TESTISLEGALACTION
	    	printf("GO8 \"\"\n");
	    #endif
			newAction(&tmp,BUILD_GO8,"L",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
	    #ifdef LOG_TESTISLEGALACTION
	    	printf("GO8 \"L\"\n");
	    #endif

	    newAction(&tmp,RETRAIN_STUDENTS,"L",STUDENT_MTV,STUDENT_BPS);
	    assert(isLegalAction(g,tmp)==TRUE);
	    newAction(&tmp,RETRAIN_STUDENTS,"asf",STUDENT_MTV,STUDENT_MTV);
	    assert(isLegalAction(g,tmp)==FALSE);
	    newAction(&tmp,RETRAIN_STUDENTS,"asf",STUDENT_THD,STUDENT_MTV);
	    assert(isLegalAction(g,tmp)==FALSE);
	    newAction(&tmp,RETRAIN_STUDENTS,"asf",STUDENT_MTV,STUDENT_THD);
	    assert(isLegalAction(g,tmp)==TRUE);

	    g->numGO8 = 8;
	    newAction(&tmp,BUILD_GO8,"",0,0);
	    assert(isLegalAction(g,tmp)==FALSE);
	    
	   disposeGame(g);
	    
	}
*/
